/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __APPCOUNTER_H
#define __APPCOUNTER_H
/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include "stm32f4xx_hal.h"
#include "timer.h"
#include "interface.h"
/* Defines  ------------------------------------------------------------------*/

#define BOOTING_ILLUSION_TIME 300
#define ILLUSION_TIME 3
#define RED_0	0x013F
#define RED_1	0x0106
#define RED_2	0x015B
#define RED_3	0x014F
#define RED_4	0x0166
#define RED_5	0x016D
#define RED_6	0x017D
#define RED_7	0x0127
#define RED_8	0x017F
#define RED_9	0x0167

#define GREEN_0	0x023F
#define GREEN_1	0x0206
#define GREEN_2	0x025B
#define GREEN_3	0x024F
#define GREEN_4	0x0266
#define GREEN_5	0x026D
#define GREEN_6	0x027D
#define GREEN_7	0x0227
#define GREEN_8	0x027F
#define GREEN_9	0x0267

#define FIRST_DIGIT		0x0400
#define SECOND_DIGIT		0x0200
#define THIRD_DIGIT		0x0100
#define FOURTH_DIGIT		0x0080
#define FIFTH_DIGIT		0x0040

#define ALL_RED_LIGHT	0x01FF
#define ALL_GREEN_LIGHT	0x02FF

#define BOOTING_STATE 0
#define BOOTING_COMPLETE 1
#define WAITING_STATE 2


void LoopAppCounter(void);




#endif /* __APPCOUNTER_H */