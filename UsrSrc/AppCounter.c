#include "AppCounter.h"


extern CONTROL_MSG ControlMsg;

uint16_t SegmentNum[20] = 
{
  /*GPIOA*/
  /*RED*/
  RED_0,       //0
  RED_1,       //1
  RED_2,       //2
  RED_3,       //3
  RED_4,       //4
  RED_5,       //5
  RED_6,       //6
  RED_7,       //7
  RED_8,       //8
  RED_9,       //9
  /*GREEN*/
  GREEN_0,       //0
  GREEN_1,       //1
  GREEN_2,       //2
  GREEN_3,       //3
  GREEN_4,       //4
  GREEN_5,       //5
  GREEN_6,       //6
  GREEN_7,       //7
  GREEN_8,       //8
  GREEN_9        //9
};

uint8_t Rest;
uint8_t Rest2;
uint8_t Rest3;
uint8_t Rest4;
uint8_t Rest5;

uint16_t SegRest;
uint16_t SegRest2;
uint16_t SegRest3;
uint16_t SegRest4;
uint16_t SegRest5;

uint8_t CounterState = 0;

uint64_t IllusionTempTick;
uint64_t IllusionTempTempTick;

void Swap(uint16_t *x,uint16_t *y)
{
  int z = *x;
  *x = *y;
  *y= z;
}

void LoadCounter(uint32_t* TotalCount)
{
//  *TotalCount = 12345;
  /*Product Count*/
  Rest = *TotalCount%10;
  Rest2 = (*TotalCount/10)%10;
  Rest3 = (*TotalCount/100)%10;
  Rest4 = (*TotalCount/1000)%10;
  Rest5 = (*TotalCount/10000)%10;
 
//  if((ControlMsg.SCMode == MODE_ERROR) && (ControlMsg.ErrCode == ERRCODE_VACCUM) || (ControlMsg.ErrCode == ERRCODE_CHUCK) || (ControlMsg.ErrCode == ERRCODE_RUNNER_PICK))
//  {
//	 SegRest = SegmentNum[Rest+10];
//	 if(*TotalCount > 9) SegRest2 = SegmentNum[Rest2+10];
//	 if(*TotalCount > 99) SegRest3 = SegmentNum[Rest3+10];
//	 if(*TotalCount > 999) SegRest4 = SegmentNum[Rest4+10];
//	 if(*TotalCount > 9999) SegRest5 = SegmentNum[Rest5+10];
//  }
//  else
  {
	 SegRest = SegmentNum[Rest];
	 if(*TotalCount > 9) SegRest2 = SegmentNum[Rest2];
	 if(*TotalCount > 99) SegRest3 = SegmentNum[Rest3];
	 if(*TotalCount > 999) SegRest4 = SegmentNum[Rest4];
	 if(*TotalCount > 9999) SegRest5 = SegmentNum[Rest5];
  }
}

void WriteSegment(uint16_t Digit, uint16_t Seg)
{
  HAL_GPIO_WritePin(GPIOF,Digit,GPIO_PIN_SET);
  HAL_GPIO_WritePin(GPIOA,Seg,GPIO_PIN_SET); 
  
}

void AllReset(void)
{
  HAL_GPIO_WritePin(GPIOF,FIRST_DIGIT,GPIO_PIN_RESET);
  HAL_GPIO_WritePin(GPIOF,SECOND_DIGIT,GPIO_PIN_RESET);
  HAL_GPIO_WritePin(GPIOF,THIRD_DIGIT,GPIO_PIN_RESET);
  HAL_GPIO_WritePin(GPIOF,FOURTH_DIGIT,GPIO_PIN_RESET);
  HAL_GPIO_WritePin(GPIOF,FIFTH_DIGIT,GPIO_PIN_RESET);
  
  HAL_GPIO_WritePin(GPIOA,SegRest,GPIO_PIN_RESET); 
  HAL_GPIO_WritePin(GPIOA,SegRest2,GPIO_PIN_RESET); 
  HAL_GPIO_WritePin(GPIOA,SegRest3,GPIO_PIN_RESET); 
  HAL_GPIO_WritePin(GPIOA,SegRest4,GPIO_PIN_RESET); 
  HAL_GPIO_WritePin(GPIOA,SegRest5,GPIO_PIN_RESET); 
  
}

void LoopBooting(void)
{
  static uint64_t BootingTempTick;
  static uint8_t SegPart = 1;

  static enum
  {
	 INIT = 0,
	 RED_SEG,
	 SWAP_SEG,
	 GREEN_SEG,
  }BootingLoopState = INIT;
  
  switch(BootingLoopState)
  {
  case INIT :
	 SegRest = 0x0101;
	 SegRest2 =	0x0101;
	 SegRest3 =	0x0101;
	 SegRest4 =	0x0101;
	 SegRest5 =	0x0101;	 
	 SetSysTick(&BootingTempTick, BOOTING_ILLUSION_TIME);
	 BootingLoopState = RED_SEG;
	 break;
	 
  case RED_SEG :
	 if(!ChkExpireSysTick(&BootingTempTick)) break;
	 SegPart = (SegPart * 2);
	 
	 SegRest = SegRest + SegPart;
	 SegRest2 =	SegRest2 + SegPart;
	 SegRest3 = SegRest3 + SegPart;
	 SegRest4 =	SegRest4 + SegPart;
	 SegRest5 =	SegRest5 + SegPart;
	 
	 if(SegRest == ALL_RED_LIGHT) BootingLoopState = GREEN_SEG;
	 SetSysTick(&BootingTempTick, BOOTING_ILLUSION_TIME);
	 break;
	 
  case GREEN_SEG:
	 if(!ChkExpireSysTick(&BootingTempTick)) break;
	 SegRest	= 	ALL_GREEN_LIGHT;
	 SegRest2 =	ALL_RED_LIGHT; 
	 SegRest3 =	ALL_RED_LIGHT; 
	 SegRest4 =	ALL_RED_LIGHT; 
	 SegRest5 =	ALL_RED_LIGHT; 
	 SetSysTick(&BootingTempTick, BOOTING_ILLUSION_TIME);
	 BootingLoopState = SWAP_SEG;
	 break;
	 
  case SWAP_SEG:
	 if(!ChkExpireSysTick(&BootingTempTick)) break;
	 if(SegRest == ALL_GREEN_LIGHT) Swap(&SegRest,&SegRest2);
	 else if(SegRest2 == ALL_GREEN_LIGHT) Swap(&SegRest2,&SegRest3);
	 else if(SegRest3 == ALL_GREEN_LIGHT) Swap(&SegRest3,&SegRest4);
	 else if(SegRest4 == ALL_GREEN_LIGHT) Swap(&SegRest4,&SegRest5);
	 else if(SegRest5 == ALL_GREEN_LIGHT) CounterState = BOOTING_COMPLETE;
	 SetSysTick(&BootingTempTick, BOOTING_ILLUSION_TIME);
	 break;
  }
}

void LoopAppCounter(void)
{
  static uint32_t TontalCntTemp = 0;
//  static uint32_t DetectFailTemp = 0;
  
  static enum
  {
	 INIT = 0,
	 WAITING,
	 FIRST_CHAR,
	 SECOND_CHAR,
	 THIRD_CHAR,
	 FOURTH_CHAR,
	 FIFTH_CHAR,
  }ChipherLoopState = INIT;
  
  switch(ChipherLoopState)
  {
  case INIT :
	 LoopBooting();
	 SetSysTick(&IllusionTempTick, ILLUSION_TIME);
	 SetSysTick(&IllusionTempTempTick, ILLUSION_TIME);
	 if(CounterState == BOOTING_COMPLETE) ChipherLoopState = WAITING;
	 else ChipherLoopState = FIRST_CHAR;
	 break;
	 
  case WAITING:	
	 if(TontalCntTemp != ControlMsg.Count.TotalCnt)
	 {
		TontalCntTemp = ControlMsg.Count.TotalCnt;
		LoadCounter(&TontalCntTemp);
	 }
//	 else if(DetectFailTemp != ControlMsg.Count.DetectFail) 
//	 {
//		if((ControlMsg.SCMode == MODE_ERROR) && (ControlMsg.ErrCode == ERRCODE_VACCUM) || (ControlMsg.ErrCode == ERRCODE_CHUCK) || (ControlMsg.ErrCode == ERRCODE_RUNNER_PICK))
//		{
//		  DetectFailTemp = ControlMsg.Count.DetectFail;
//		  LoadCounter(&DetectFailTemp);
//		}
//	 }
//	 else if((TontalCntTemp == 0) && (DetectFailTemp == 0)) LoadCounter(&TontalCntTemp);
	 else if(TontalCntTemp == 0) LoadCounter(&TontalCntTemp);
	 
	 SetSysTick(&IllusionTempTick, ILLUSION_TIME);
	 SetSysTick(&IllusionTempTempTick, ILLUSION_TIME);
	 CounterState = WAITING_STATE;
	 ChipherLoopState = FIRST_CHAR;
	 break;
	 
  case FIRST_CHAR:
	 if(!ChkExpireSysTick(&IllusionTempTick)) break;
	 AllReset();
	 if(!ChkExpireSysTick(&IllusionTempTempTick)) break;
	 WriteSegment(FIRST_DIGIT,SegRest);
	 SetSysTick(&IllusionTempTick, ILLUSION_TIME);
	 SetSysTick(&IllusionTempTempTick, ILLUSION_TIME);
	 if ((CounterState == WAITING_STATE) && (TontalCntTemp < 10)) ChipherLoopState = WAITING;
	 else ChipherLoopState = SECOND_CHAR;
	 break;
	 
  case SECOND_CHAR:
	 if(!ChkExpireSysTick(&IllusionTempTick)) break;
	 AllReset();
	 if(!ChkExpireSysTick(&IllusionTempTempTick)) break;
	 WriteSegment(SECOND_DIGIT,SegRest2);
	 SetSysTick(&IllusionTempTick, ILLUSION_TIME);
	 SetSysTick(&IllusionTempTempTick, ILLUSION_TIME);
	 if ((CounterState == WAITING_STATE) && (TontalCntTemp < 100)) ChipherLoopState = WAITING;
	 else ChipherLoopState = THIRD_CHAR;
	 break;
	 
  case THIRD_CHAR:
	 if(!ChkExpireSysTick(&IllusionTempTick)) break;
	 AllReset();
	 if(!ChkExpireSysTick(&IllusionTempTempTick)) break;
	 WriteSegment(THIRD_DIGIT,SegRest3);
	 SetSysTick(&IllusionTempTick, ILLUSION_TIME);
	 SetSysTick(&IllusionTempTempTick, ILLUSION_TIME);
	 if ((CounterState == WAITING_STATE) && (TontalCntTemp < 1000)) ChipherLoopState = WAITING;
	 else ChipherLoopState = FOURTH_CHAR;
	 break;
	 
  case FOURTH_CHAR:
	 if(!ChkExpireSysTick(&IllusionTempTick)) break;
	 AllReset();
	 if(!ChkExpireSysTick(&IllusionTempTempTick)) break;
	 WriteSegment(FOURTH_DIGIT,SegRest4);
	 SetSysTick(&IllusionTempTick, ILLUSION_TIME);
	 SetSysTick(&IllusionTempTempTick, ILLUSION_TIME);
	 if ((CounterState == WAITING_STATE) && (TontalCntTemp < 10000)) ChipherLoopState = WAITING;
	 else ChipherLoopState = FIFTH_CHAR;
	 break;
	 
  case FIFTH_CHAR:
	 if(!ChkExpireSysTick(&IllusionTempTick)) break;
	 AllReset();
	 if(!ChkExpireSysTick(&IllusionTempTempTick)) break;
	 WriteSegment(FIFTH_DIGIT,SegRest5);
	 SetSysTick(&IllusionTempTick, ILLUSION_TIME);
	 SetSysTick(&IllusionTempTempTick, ILLUSION_TIME);
	 if (CounterState == BOOTING_STATE) ChipherLoopState = INIT;
	 else if (CounterState == WAITING_STATE) ChipherLoopState = WAITING;
	 break;
  }
}