#include "AppKey.h"
KeyTypeDef Key;

void LoopAppKey(void)
{
	static uint64_t TempTick = 0;
	static uint16_t ValueKey[5] = {0,};
	static uint8_t	NowRow = 0;
	
	static enum
	{
		INIT = 0,
		
		STATE_READY_KEY,
		STATE_SET_ROW,
		STATE_READ_KEY,
		STATE_DO_KEY,
		
	}KeyLoopState = INIT; 
	
	switch(KeyLoopState)
	{
		
		case INIT:
		
		SetSysTick(&TempTick, TIME_KEY);
		KeyLoopState = STATE_READY_KEY;
		
		break;
		
		case STATE_READY_KEY:
		if(ChkExpireSysTick(&TempTick))
		{
			ResetKey();
			KeyLoopState = STATE_SET_ROW;
		}
		break;
		
		case STATE_SET_ROW:
			RowKey(NowRow);
			KeyLoopState = STATE_READ_KEY;
		
		case STATE_READ_KEY:
			ValueKey[NowRow] = ReadKey();
			NowRow++;
			KeyLoopState = STATE_READY_KEY;
			
			if(NowRow >= MAX_ROW)
			{
				NowRow = 0;
				KeyLoopState = STATE_DO_KEY;
			}
			
		break;
		
		case STATE_DO_KEY:
		
		Key = KEY_NONE;
		for(int i=0;i<MAX_ROW;i++)
		{
			if(ValueKey[i] != 0) 
			{
				Key |= (ValueKey[i] << (i * MAX_ROW));
			}
		}
		
		for(int i=0;i<MAX_ROW;i++) ValueKey[i] = 0;
		
//		if(Key != KEY_NONE)
//		{
//			for(int i=0;i<25;i++)
//			{
//				if((Key >> i) & 0x00000001)
//					printf("%d\t",i);
//			}
//			printf("\r");
//		}
		
		SetSysTick(&TempTick, TIME_KEY);
		KeyLoopState = STATE_READY_KEY;
		
		break;
		
	}
}	
uint32_t GetKey(void)
{
	static uint32_t tempKey;
	if(Key == tempKey) Key = KEY_NONE;
	else
	{
		tempKey = Key;
	}
	return Key;
}