#include "AppSeq.h"
#include "interface.h"

extern CONTROL_MSG ControlMsg;
//extern ROBOT_CONFIG  RobotCfg;
//extern SEQ_MSG SeqMsg;
extern IO_MSG IOMsg;
//extern SEQ_MSG* pSeqMsg;

uint8_t fSeqEnd;

//void CycleSeq(void);
//void PrintfSeqMsg(SEQ_MSG* pSeqMsgFunc);
//  static uint64_t SeqTempTick;

void CmdReady(void)
{
  //Interlock
  IOMsg.Y2A_MoldOpenClose = SIGNAL_ON;
  IOMsg.Y2B_Ejector = SIGNAL_ON;
  IOMsg.Y29_CycleStart = SIGNAL_OFF;
  
  //Output
  IOMsg.Y20_Down = SIGNAL_OFF;
  IOMsg.Y2D_SubUp = SIGNAL_OFF;
  IOMsg.Y24_ChuckRotation = SIGNAL_OFF; 
  
//  //OutsideWait
//  if (RobotCfg.MotionMode.OutsideWait == USE)
//  {
//    IOMsg.Y23_Swing = SIGNAL_ON;
//    IOMsg.Y2F_SwingReturn = SIGNAL_OFF;
//  }
//  else if (RobotCfg.MotionMode.OutsideWait == NO_USE)
//  {
//    IOMsg.Y23_Swing = SIGNAL_OFF;
//    IOMsg.Y2F_SwingReturn = SIGNAL_ON;
//  }
  
//  //Kick
//  if (!(RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_SUB))
//  {
//    if (RobotCfg.MotionMode.MainArmType == ARM_L_TYPE)
//    {
//      if (RobotCfg.MotionMode.MainArmDownPos == ARM_DOWNPOS_CLAMP)
//        IOMsg.Y21_Kick = SIGNAL_OFF;
//      else if (RobotCfg.MotionMode.MainArmDownPos == ARM_DOWNPOS_NOZZLE)
//        IOMsg.Y21_Kick = SIGNAL_ON;
//    }
//    else if ((RobotCfg.MotionMode.MainArmType == ARM_U_TYPE) || (RobotCfg.MotionMode.MainArmType == ARM_I_TYPE))
//    {
//      if (RobotCfg.MotionMode.MainArmDownPos == ARM_DOWNPOS_CLAMP)
//        IOMsg.Y21_Kick = SIGNAL_ON;
//      else if (RobotCfg.MotionMode.MainArmDownPos == ARM_DOWNPOS_NOZZLE)
//        IOMsg.Y21_Kick = SIGNAL_OFF;
//    }
//  }
//  if (!(RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MAIN))
//  {
//    if (RobotCfg.MotionMode.SubArmType == ARM_L_TYPE)
//    {
//      if (RobotCfg.MotionMode.SubArmDownPos == ARM_DOWNPOS_CLAMP)
//        IOMsg.Y2E_SubKick = SIGNAL_ON;
//      else if (RobotCfg.MotionMode.SubArmDownPos == ARM_DOWNPOS_NOZZLE)
//        IOMsg.Y2E_SubKick = SIGNAL_OFF;
//    }
//    else if ((RobotCfg.MotionMode.SubArmType == ARM_U_TYPE) || (RobotCfg.MotionMode.SubArmType == ARM_I_TYPE))
//    {
//      if (RobotCfg.MotionMode.SubArmDownPos == ARM_DOWNPOS_CLAMP)
//        IOMsg.Y2E_SubKick = SIGNAL_OFF;
//      else if (RobotCfg.MotionMode.SubArmDownPos == ARM_DOWNPOS_NOZZLE)
//        IOMsg.Y2E_SubKick = SIGNAL_ON;
//    }
//  }
  
  IOMsg.Y2C_Conveyor = SIGNAL_OFF;
  IOMsg.Y22_Chuck = SIGNAL_OFF;
  IOMsg.Y25_Vaccum = SIGNAL_OFF;
  IOMsg.Y26_Nipper = SIGNAL_OFF;
  IOMsg.Y27_SubGrip = SIGNAL_OFF;
  IOMsg.Y28_Alarm = SIGNAL_OFF;
  
  /************************ Mode Test ************************/
//  RobotCfg.MotionMode.MotionArm = ROBOT_MAIN_MAIN;
//  RobotCfg.MotionMode.MainChuck = NO_USE;
//  RobotCfg.MotionMode.MainVaccum = USE;
//  RobotCfg.MotionMode.MainRotateChuck = USE;
//  RobotCfg.MotionMode.OutsideWait = NO_USE;
//  RobotCfg.MotionMode.MainArmType = ARM_L_TYPE;
//  RobotCfg.MotionMode.SubArmType = ARM_NO_USE;
//  RobotCfg.MotionMode.MainArmDownPos = ARM_DOWNPOS_NOZZLE;
//  RobotCfg.MotionMode.SubArmDownPos = ARM_DOWNPOS_NO_USE;
//  RobotCfg.MotionMode.ChuckOff = RELEASE_POS_NO_USE;
//  RobotCfg.MotionMode.VaccumOff = RELEASE_POS_OUTSIDE;
//  RobotCfg.MotionMode.SubArmOff = RELEASE_POS_NO_USE;
//  RobotCfg.MotionMode.ChuckRejectPos = RELEASE_POS_NO_USE;
//  RobotCfg.MotionMode.VaccumRejectPos = RELEASE_POS_NO_USE;
//  RobotCfg.MotionMode.MainNipper = NO_USE;
//  
//  RobotCfg.Function.Detection = FUNC_OFF;
//  RobotCfg.Function.Ejector = FUNC_ON;
//  RobotCfg.Function.Reject = FUNC_OFF;
//  
//  RobotCfg.Setting.ErrorTime = 30;
//  RobotCfg.Setting.ItlSaftyDoor = USE;
//  RobotCfg.Setting.ItlAutoInjection = NO_USE;
//  RobotCfg.Setting.ItlFullAuto = USE;
//  RobotCfg.Setting.ItlReject = USE;
//  
//  RobotCfg.MotionDelay.DownDelay = 50;
//  RobotCfg.MotionDelay.KickDelay = 40;
//  RobotCfg.MotionDelay.EjectorDelay = 30;
//  RobotCfg.MotionDelay.ChuckDelay = 20;
//  RobotCfg.MotionDelay.KickReturnDelay = 10;
//  RobotCfg.MotionDelay.UpDelay = 60;
//  RobotCfg.MotionDelay.SwingDelay = 70;
//  RobotCfg.MotionDelay.Down2ndDelay = 80;
//  RobotCfg.MotionDelay.OpenDelay = 90;
//  RobotCfg.MotionDelay.Up2ndDelay = 10;
//  RobotCfg.MotionDelay.ChuckRotateReturnDelay = 50;
//  RobotCfg.MotionDelay.SwingReturnDelay = 60;
//  RobotCfg.MotionDelay.NipperOnDelay = 80;
//  RobotCfg.MotionDelay.ConveyorDelay = 20;
  
  IOMsg.X18_MoldOpenComplete = SIGNAL_ON;
  IOMsg.X1H_FullAuto = SIGNAL_ON;
  /**********************************************************/
}


//void DispSequence(void)
//{
//  //DOWN
//  if (RobotCfg.MotionMode.OutsideWait == USE)
//  {
//    SeqMsg.SeqDispArr[SeqMsg.SeqDispPos] = SEQ_SWING_RETURN;
//    SeqMsg.SeqTime[SeqMsg.SeqDispPos++] = RobotCfg.MotionDelay.SwingReturnDelay;
//  }
//  SeqMsg.SeqDispArr[SeqMsg.SeqDispPos] = SEQ_DOWN;
//  SeqMsg.SeqTime[SeqMsg.SeqDispPos++] = RobotCfg.MotionDelay.DownDelay;
//  
//  
//  //Kick L type
//  if ((!(RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_SUB) && (RobotCfg.MotionMode.MainArmType == ARM_L_TYPE)) ||
//      (!(RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MAIN) && (RobotCfg.MotionMode.SubArmType == ARM_L_TYPE)))
//  {
//    SeqMsg.SeqDispArr[SeqMsg.SeqDispPos] = SEQ_KICK;
//    SeqMsg.SeqTime[SeqMsg.SeqDispPos++] = RobotCfg.MotionDelay.KickDelay;
//  }
//  
//  //TakeOut
//  if (RobotCfg.Function.Ejector == FUNC_ON)
//  {
//    SeqMsg.SeqDispArr[SeqMsg.SeqDispPos] = SEQ_EJECT;
//    SeqMsg.SeqTime[SeqMsg.SeqDispPos++] = RobotCfg.MotionDelay.EjectorDelay;
//  }
//  SeqMsg.SeqDispArr[SeqMsg.SeqDispPos] = SEQ_CHUCK_ON;
//  SeqMsg.SeqTime[SeqMsg.SeqDispPos++] = RobotCfg.MotionDelay.ChuckDelay;
//  
//  //Kick L type, U type Return
//  if ((RobotCfg.MotionMode.MainArmType == ARM_L_TYPE) || (RobotCfg.MotionMode.SubArmType == ARM_L_TYPE)
//      || (RobotCfg.MotionMode.MainArmType == ARM_U_TYPE) || (RobotCfg.MotionMode.SubArmType == ARM_U_TYPE))
//  {
//    SeqMsg.SeqDispArr[SeqMsg.SeqDispPos] = SEQ_KICK_RETURN;
//    SeqMsg.SeqTime[SeqMsg.SeqDispPos++] = RobotCfg.MotionDelay.KickReturnDelay;
//  }
//  
//  //InMold Release
//  if ((RobotCfg.MotionMode.ChuckOff == RELEASE_POS_INMOLD) || (RobotCfg.MotionMode.VaccumOff == RELEASE_POS_INMOLD)     //Main Release
//      || ((RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_INMOLD)&&(IOMsg.X1B_Reject == SIGNAL_ON)) 
//        || ((RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_INMOLD)&&(IOMsg.X1B_Reject == SIGNAL_ON))      //Reject Release
//          ||(RobotCfg.MotionMode.SubArmOff == RELEASE_POS_INMOLD))            //Sub Release
//  {
//    SeqMsg.SeqDispArr[SeqMsg.SeqDispPos] = SEQ_CHUCK_OFF;
//    SeqMsg.SeqTime[SeqMsg.SeqDispPos++] = RobotCfg.MotionDelay.OpenDelay;
//  }
//  
//  //UP
//  SeqMsg.SeqDispArr[SeqMsg.SeqDispPos] = SEQ_UP;
//  SeqMsg.SeqTime[SeqMsg.SeqDispPos++] = RobotCfg.MotionDelay.UpDelay;
//  
//  //Kick MS L type
//  if ((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS) &&
//      ((RobotCfg.MotionMode.MainArmType == ARM_L_TYPE) || (RobotCfg.MotionMode.SubArmType == ARM_L_TYPE)))
//  {
//    SeqMsg.SeqDispArr[SeqMsg.SeqDispPos] = SEQ_KICK;
//    SeqMsg.SeqTime[SeqMsg.SeqDispPos++] = 0;
//  }
//  
//  //Rotation
//  SeqMsg.SeqDispArr[SeqMsg.SeqDispPos] = SEQ_SWING;
//  SeqMsg.SeqTime[SeqMsg.SeqDispPos++] = RobotCfg.MotionDelay.SwingDelay;
//  
//  //Chuck Rotation
//  if (RobotCfg.MotionMode.MainRotateChuck == USE)
//  {
//    SeqMsg.SeqDispArr[SeqMsg.SeqDispPos] = SEQ_CHUCK_ROTATE;
//    SeqMsg.SeqTime[SeqMsg.SeqDispPos++] = RobotCfg.MotionDelay.ChuckRotateReturnDelay;
//  }
//  
//  //Rotation Release
//  if ((RobotCfg.MotionMode.ChuckOff == RELEASE_POS_OUTSIDE) || (RobotCfg.MotionMode.VaccumOff == RELEASE_POS_OUTSIDE)     //Main Release
//      || ((RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_OUTSIDE)&&(IOMsg.X1B_Reject == SIGNAL_ON))
//        || ((RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_OUTSIDE)&&(IOMsg.X1B_Reject == SIGNAL_ON))      //Reject Release
//          ||(RobotCfg.MotionMode.SubArmOff == RELEASE_POS_OUTSIDE))        //Sub Release
//  {
//    SeqMsg.SeqDispArr[SeqMsg.SeqDispPos] = SEQ_CHUCK_OFF;
//    SeqMsg.SeqTime[SeqMsg.SeqDispPos++] = RobotCfg.MotionDelay.OpenDelay;
//  }
//  
//  //2nd Down
//  SeqMsg.SeqDispArr[SeqMsg.SeqDispPos] = SEQ_DOWN_2ND;
//  SeqMsg.SeqTime[SeqMsg.SeqDispPos++] = RobotCfg.MotionDelay.Down2ndDelay;
//  
//  //Nipper ON
//  if ((RobotCfg.MotionMode.MainNipper == USE) && (RobotCfg.MotionMode.MainChuck == USE) && (RobotCfg.MotionMode.MainVaccum == NO_USE))
//  {
//    SeqMsg.SeqDispArr[SeqMsg.SeqDispPos] = SEQ_NIPPER_CUT;
//    SeqMsg.SeqTime[SeqMsg.SeqDispPos++] = RobotCfg.MotionDelay.NipperOnDelay;
//  }
//  
//  //2nd Down Release
//  if ((RobotCfg.MotionMode.ChuckOff == RELEASE_POS_2ndDOWN) || (RobotCfg.MotionMode.VaccumOff == RELEASE_POS_2ndDOWN)     //Main Release
//      || ((RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_2ndDOWN)&&(IOMsg.X1B_Reject == SIGNAL_ON))
//        || ((RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_2ndDOWN)&&(IOMsg.X1B_Reject == SIGNAL_ON))      //Reject Release
//          ||(RobotCfg.MotionMode.SubArmOff == RELEASE_POS_2ndDOWN))        //Sub Release
//  {
//    SeqMsg.SeqDispArr[SeqMsg.SeqDispPos] = SEQ_CHUCK_OFF;
//    SeqMsg.SeqTime[SeqMsg.SeqDispPos++] = RobotCfg.MotionDelay.OpenDelay;
//  }
//  
//  //2nd UP
//  SeqMsg.SeqDispArr[SeqMsg.SeqDispPos] = SEQ_UP_2ND;
//  SeqMsg.SeqTime[SeqMsg.SeqDispPos++] = RobotCfg.MotionDelay.Up2ndDelay;
//  
//  //2nd UP Release
//  if ((RobotCfg.MotionMode.ChuckOff == RELEASE_POS_2ndUP) || (RobotCfg.MotionMode.VaccumOff == RELEASE_POS_2ndUP)     //Main Release
//      || ((RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_2ndUP)&&(IOMsg.X1B_Reject == SIGNAL_ON))
//        || ((RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_2ndUP)&&(IOMsg.X1B_Reject == SIGNAL_ON))      //Reject Release
//          ||(RobotCfg.MotionMode.SubArmOff == RELEASE_POS_2ndUP))        //Sub Release
//  {
//    SeqMsg.SeqDispArr[SeqMsg.SeqDispPos] = SEQ_CHUCK_OFF;
//    SeqMsg.SeqTime[SeqMsg.SeqDispPos++] = RobotCfg.MotionDelay.OpenDelay;
//  }
//  
//  //Chuck Rotation Return
//  if (RobotCfg.MotionMode.MainRotateChuck == USE)
//  {
////    SeqMsg.SeqDispArr[SeqMsg.SeqDispPos] = SEQ_CHUCK_ROTATE_RETURN;
//    SeqMsg.SeqTime[SeqMsg.SeqDispPos++] = RobotCfg.MotionDelay.SwingReturnDelay;
//  }
//  
//  if (RobotCfg.MotionMode.OutsideWait == NO_USE)
//  {
//    SeqMsg.SeqDispArr[SeqMsg.SeqDispPos] =   SEQ_SWING_RETURN;
//    SeqMsg.SeqTime[SeqMsg.SeqDispPos++] = RobotCfg.MotionDelay.SwingReturnDelay;
//  }
//  
//  if ((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS) && 
//      ((RobotCfg.MotionMode.MainArmType == ARM_L_TYPE) || (RobotCfg.MotionMode.MainArmType == ARM_U_TYPE)))
//  {
//    SeqMsg.SeqDispArr[SeqMsg.SeqDispPos] = SEQ_KICK_RETURN;
//    SeqMsg.SeqTime[SeqMsg.SeqDispPos++] = 0;
//  }
//  SeqMsg.SeqDispArr[SeqMsg.SeqDispPos] = SEQ_DISP_END;
//}

//void AutoSeq(void)
//{
//  
//}

//void CycleSeq(void)
//{
//  static uint64_t SeqTempTick;
//  //  uint8_t* pMotionDelay;
//  static uint64_t DelayTempTick;
//  static enum
//  {
//    INIT_SEQ_LOOP = 0,
//    PROCESS_SEQ,
//    OUTPUT,
//    WAIT_INPUT,
//    MOTION_DELAY,
//    PROCESS_DONE,
//    //    SEND_SEQ, 
//  }SeqLoopState = INIT_SEQ_LOOP;
//  
//  switch(SeqLoopState)
//  {
//  case INIT_SEQ_LOOP:
//    printf("INIT_SEQ \n\r");                  //Debug
//    SeqMsg.SeqPos = 0;
//    SeqMsg.SeqDispPos = 0;
//    SeqLoopState = PROCESS_SEQ;
//    
//    ///////////////////////////// test //////////////////////////////
//    //    IOMsg.X1I_EMOFromIMM = SIGNAL_OFF;                       //
//    //    SeqMsg.SeqArr[1] = DELAY_SWING;                          //
//    //    SeqMsg.SeqArr[2] = OUTPUT_SWING_Y23_ON;                  //
//    //    SeqMsg.SeqArr[3] = DELAY_SWING_RETURN;                   //
//    //    SeqMsg.SeqArr[4] = INPUT_SWING_RETURN_DONE_X15_ON;       //
//    //    SeqMsg.SeqArr[5] = OUTPUT_SUB_CHUCK_Y27_ON;              //
//    //    SeqMsg.SeqArr[6] = INPUT_SUB_CHUCK_OK_X1F_ON;            //
//    /////////////////////////////////////////////////////////////////    
//    break;
//    
//  case PROCESS_SEQ:
//    printf("PROCESS_SEQ \n\r");                  //Debug     
//    if (((SEQ_OUTPUT_ON_START <= SeqMsg.SeqArr[SeqMsg.SeqPos]) && (SeqMsg.SeqArr[SeqMsg.SeqPos] <= SEQ_OUTPUT_INTERLOCK_ON_END))
//        || ((SEQ_OUTPUT_OFF_START <= SeqMsg.SeqArr[SeqMsg.SeqPos]) && (SeqMsg.SeqArr[SeqMsg.SeqPos] <= SEQ_OUTPUT_INTERLOCK_OFF_END)))
//      SeqLoopState = OUTPUT;
//    else if (((SEQ_INPUT_ON_START <= SeqMsg.SeqArr[SeqMsg.SeqPos]) && (SeqMsg.SeqArr[SeqMsg.SeqPos] <= SEQ_INPUT_INTERLOCK_ON_END))
//             || ((SEQ_INPUT_OFF_START <= SeqMsg.SeqArr[SeqMsg.SeqPos]) && (SeqMsg.SeqArr[SeqMsg.SeqPos] <= SEQ_INPUT_INTERLOCK_OFF_END)))
//      SeqLoopState = WAIT_INPUT;
//    else if (SEQ_ENUM_DELAY <= SeqMsg.SeqArr[SeqMsg.SeqPos]) 
//      SeqLoopState = MOTION_DELAY;
//    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == SEQ_ENUM_END) 
//    {
//      SeqLoopState = PROCESS_DONE;
//      fSeqEnd = 1;
//    } 
//    printf("SeqMsg.SeqArr[%d] : %d\n\r",SeqMsg.SeqPos,SeqMsg.SeqArr[SeqMsg.SeqPos]);                              //Debug
//    break;
//    
//  case OUTPUT:
//    /********************************************************  OUTPUT  ********************************************************/  
//    printf("OUTPUT \n\r");                  //Debug
//    //Output ON
//    if((SEQ_OUTPUT_ON_START <= SeqMsg.SeqArr[SeqMsg.SeqPos]) && (SeqMsg.SeqArr[SeqMsg.SeqPos] <= SEQ_OUTPUT_INTERLOCK_ON_END))
//    {
//      (*(&(IOMsg.Y20_Down)+(SeqMsg.SeqArr[SeqMsg.SeqPos]-SEQ_OUTPUT_ON_START))) = SIGNAL_ON;
//      printf("SignalON\n\r");
//    }
//    
//    //Output OFF
//    if((SEQ_OUTPUT_OFF_START <= SeqMsg.SeqArr[SeqMsg.SeqPos]) && (SeqMsg.SeqArr[SeqMsg.SeqPos] <= SEQ_OUTPUT_INTERLOCK_OFF_END))
//    {
//      (*(&(IOMsg.Y20_Down)+(SeqMsg.SeqArr[SeqMsg.SeqPos]-SEQ_OUTPUT_OFF_START))) = SIGNAL_OFF;
//      printf("SignalOFF\n\r");
//    }    
//    printf("SeqMsg.SeqArr[%d] : %d\n\r",SeqMsg.SeqPos,SeqMsg.SeqArr[SeqMsg.SeqPos]);                              //Debug
//    SeqLoopState = PROCESS_DONE;
//    break;
//    
//    //    if (SeqMsg.SeqArr[SeqMsg.SeqPos] == OUTPUT_UPDOWN_Y20_ON)
//    //      IOMsg.Y20_Down = SIGNAL_ON;
//    //    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == OUTPUT_KICK_Y21_ON)
//    //      IOMsg.Y21_Kick = SIGNAL_ON;
//    //    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == OUTPUT_CHUCK_Y22_ON)
//    //      IOMsg.Y22_Chuck = SIGNAL_ON;
//    //    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == OUTPUT_SWING_Y23_ON)
//    //      IOMsg.Y23_Swing = SIGNAL_ON;
//    //    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == OUTPUT_SWING_RETURN_Y2F_ON)
//    //      IOMsg.Y2F_SwingReturn = SIGNAL_ON;
//    //    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == OUTPUT_ROTATE_CHUCK_Y24_ON)
//    //      IOMsg.Y24_ChuckRotation = SIGNAL_ON;
//    //    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == OUTPUT_VACCUM_Y25_ON)
//    //      IOMsg.Y25_Vaccum = SIGNAL_ON;
//    //    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == OUTPUT_NIPPER_Y26_ON)
//    //      IOMsg.Y26_Nipper = SIGNAL_ON;
//    //    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == OUTPUT_SUB_UPDOWN_Y2D_ON)
//    //      IOMsg.Y2D_SubUp = SIGNAL_ON;
//    //    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == OUTPUT_SUB_KICK_Y2E_ON)
//    //      IOMsg.Y2E_SubKick = SIGNAL_ON;
//    //    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == OUTPUT_SUB_CHUCK_Y27_ON)
//    //      IOMsg.Y27_SubGrip = SIGNAL_ON;
//    //    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == OUTPUT_ALARM_Y28_ON)
//    //      IOMsg.Y28_Alarm = SIGNAL_ON;
//    //    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == OUTPUT_MAIN_POWER_Y2G_ON)
//    //      IOMsg.Y2G_MainPower = SIGNAL_ON;
//    //    
//    //Output InterLock ON
//    //    if (SeqMsg.SeqArr[SeqMsg.SeqPos] == ITLO_CYCLE_START_Y29_ON)
//    //      IOMsg.Y29_CycleStart = SIGNAL_ON;
//    //    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == ITLO_MOLD_OPENCLOSE_Y2A_ON)
//    //      IOMsg.Y2A_MoldOpenClose = SIGNAL_ON;
//    //    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == ITLO_EJECTOR_Y2B_ON)
//    //      IOMsg.Y2B_Ejector = SIGNAL_ON;
//    //    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == ITLO_CONVEYOR_Y2C_ON)
//    //      IOMsg.Y2C_Conveyor = SIGNAL_ON;
//    //    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == ITLO_BUZZER_Y28_ON)
//    //      IOMsg.Y28_Buzzer = SIGNAL_ON; 
//    
//    //Output OFF
//    //    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == OUTPUT_UPDOWN_Y20_OFF)
//    //      IOMsg.Y20_Down = SIGNAL_OFF;
//    //    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == OUTPUT_KICK_Y21_OFF)
//    //      IOMsg.Y21_Kick = SIGNAL_OFF;
//    //    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == OUTPUT_CHUCK_Y22_OFF)
//    //      IOMsg.Y22_Chuck = SIGNAL_OFF;
//    //    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == OUTPUT_SWING_Y23_OFF)
//    //      IOMsg.Y23_Swing = SIGNAL_OFF;
//    //    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == OUTPUT_SWING_RETURN_Y2F_OFF)
//    //      IOMsg.Y2F_SwingReturn = SIGNAL_OFF;
//    //    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == OUTPUT_ROTATE_CHUCK_Y24_OFF)
//    //      IOMsg.Y24_ChuckRotation = SIGNAL_OFF;
//    //    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == OUTPUT_VACCUM_Y25_OFF)
//    //      IOMsg.Y25_Vaccum = SIGNAL_OFF;
//    //    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == OUTPUT_NIPPER_Y26_OFF)
//    //      IOMsg.Y26_Nipper = SIGNAL_OFF;
//    //    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == OUTPUT_SUB_UPDOWN_Y2D_OFF)
//    //      IOMsg.Y2D_SubUp = SIGNAL_OFF;
//    //    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == OUTPUT_SUB_KICK_Y2E_OFF)
//    //      IOMsg.Y2E_SubKick = SIGNAL_OFF;
//    //    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == OUTPUT_SUB_CHUCK_Y27_OFF)
//    //      IOMsg.Y27_SubGrip = SIGNAL_OFF;
//    //    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == OUTPUT_ALARM_Y28_OFF)
//    //      IOMsg.Y28_Alarm = SIGNAL_OFF;
//    //    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == OUTPUT_MAIN_POWER_Y2G_OFF)
//    //      IOMsg.Y2G_MainPower = SIGNAL_OFF;
//    //    //Output Interlock OFF
//    //    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == ITLO_CYCLE_START_Y29_OFF)
//    //      IOMsg.Y29_CycleStart = SIGNAL_OFF;
//    //    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == ITLO_MOLD_OPENCLOSE_Y2A_OFF)
//    //      IOMsg.Y2A_MoldOpenClose = SIGNAL_OFF;
//    //    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == ITLO_EJECTOR_Y2B_OFF)
//    //      IOMsg.Y2B_Ejector = SIGNAL_OFF;
//    //    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == ITLO_CONVEYOR_Y2C_OFF)
//    //      IOMsg.Y2C_Conveyor = SIGNAL_OFF;
//    //    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == ITLO_BUZZER_Y28_OFF)
//    //      IOMsg.Y28_Buzzer = SIGNAL_OFF;
//    
//
//    /********************************************************  INPUT  *********************************************************/
//  case WAIT_INPUT:
//    printf("INPUT \n\r");                  //Debug
//    //Input ON
//    if((SEQ_INPUT_ON_START <= SeqMsg.SeqArr[SeqMsg.SeqPos]) && (SeqMsg.SeqArr[SeqMsg.SeqPos] <= SEQ_INPUT_INTERLOCK_ON_END))
//    {
//      if (((SeqMsg.SeqArr[SeqMsg.SeqPos] == INPUT_CHUCK_OK_X16_ON) && (IOMsg.X16_MainChuckOk != SIGNAL_ON)) || 
//          ((SeqMsg.SeqArr[SeqMsg.SeqPos] == INPUT_VACCUM_CHECK_X17_ON) && (IOMsg.X17_MainVaccumOk != SIGNAL_ON)) || 
//            ((SeqMsg.SeqArr[SeqMsg.SeqPos] == INPUT_SUB_CHUCK_OK_X1F_ON) && (IOMsg.X1F_SubGripComplete != SIGNAL_ON)))
//      {
//        SetSysTick(&SeqTempTick, TAKEOUT_CHECK);
//        printf("Delay Time : TAKEOUT_CHECK \n\r");
//      }
//      else if (*(&(IOMsg.X11_MainArmUpComplete)+(SeqMsg.SeqArr[SeqMsg.SeqPos]-SEQ_INPUT_ON_START)) != SIGNAL_ON)
//      {
//        SetSysTick(&SeqTempTick, RobotCfg.Setting.ErrorTime*DELAY_TIME);
//        printf("Delay Time : ErrorTime_ON\n\r");
//      }
//      else if (*(&(IOMsg.X1H_FullAuto)+(SeqMsg.SeqArr[SeqMsg.SeqPos]-SEQ_INPUT_INTERLOCK_ON_START)) != SIGNAL_ON) break;
//    }
//
//    //Input OFF
//    if((SEQ_INPUT_OFF_START <= SeqMsg.SeqArr[SeqMsg.SeqPos]) && (SeqMsg.SeqArr[SeqMsg.SeqPos] <= SEQ_INPUT_INTERLOCK_OFF_END))
//    {
//      if (*(&(IOMsg.X11_MainArmUpComplete)+(SeqMsg.SeqArr[SeqMsg.SeqPos]-SEQ_INPUT_OFF_START)) == SIGNAL_OFF) //test  != SIGNAL_OFF
//      {
//        SetSysTick(&SeqTempTick, RobotCfg.Setting.ErrorTime*DELAY_TIME);
//        printf("Delay Time : ErrorTime_OFF\n\r");
//      }
//      else if (*(&(IOMsg.X1H_FullAuto)+(SeqMsg.SeqArr[SeqMsg.SeqPos]-SEQ_INPUT_INTERLOCK_OFF_START)) != SIGNAL_OFF) break;
//    }
//    printf("SeqMsg.SeqArr[%d] : %d\n\r",SeqMsg.SeqPos,SeqMsg.SeqArr[SeqMsg.SeqPos]);    //Debug   
//    SeqLoopState = PROCESS_DONE;
//    break;
//
////    //Input ON
////    if (SeqMsg.SeqArr[SeqMsg.SeqPos] == INPUT_UP_COMPLETE_X11_ON) 
////    {
////      if (IOMsg.X11_MainArmUpComplete != SIGNAL_ON) SetSysTick(&SeqTempTick, RobotCfg.Setting.ErrorTime*DELAY_TIME);             //test
////    }
////    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == INPUT_SWING_DONE_X14_ON)
////    {
////      if (IOMsg.X14_SwingOk != SIGNAL_ON) SetSysTick(&SeqTempTick, RobotCfg.Setting.ErrorTime*DELAY_TIME);             //test
////    }
////    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == INPUT_SWING_RETURN_DONE_X15_ON)
////    {
////      if (IOMsg.X15_SwingReturnOk != SIGNAL_ON) SetSysTick(&SeqTempTick, RobotCfg.Setting.ErrorTime*DELAY_TIME);             //test
////    }
////    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == INPUT_SUB_UP_COMPLETE_X1G_ON)
////    {
////      if (IOMsg.X1G_SubUpComplete != SIGNAL_ON) SetSysTick(&SeqTempTick, RobotCfg.Setting.ErrorTime*DELAY_TIME);             //test
////    }
////    //Input OFF
////    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == INPUT_UP_COMPLETE_X11_OFF) 
////    {
////      if (IOMsg.X11_MainArmUpComplete != SIGNAL_OFF) SetSysTick(&SeqTempTick, RobotCfg.Setting.ErrorTime*DELAY_TIME);           //test
////    }
////    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == INPUT_SWING_DONE_X14_OFF)
////    {
////      if (IOMsg.X14_SwingOk != SIGNAL_OFF) SetSysTick(&SeqTempTick, RobotCfg.Setting.ErrorTime*DELAY_TIME);             //test   
////    }
////    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == INPUT_SWING_RETURN_DONE_X15_OFF)
////    {
////      if (IOMsg.X15_SwingReturnOk != SIGNAL_OFF) SetSysTick(&SeqTempTick, RobotCfg.Setting.ErrorTime*DELAY_TIME);             //test
////    }
////    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == INPUT_SUB_UP_COMPLETE_X1G_OFF)
////    {
////      if (IOMsg.X1G_SubUpComplete != SIGNAL_OFF) SetSysTick(&SeqTempTick, RobotCfg.Setting.ErrorTime*DELAY_TIME);             //test
////    }
////    
//
//    
////    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == ITLI_FULLAUTO_X1H_ON) 
////    {
////      if (IOMsg.X1H_FullAuto != SIGNAL_ON) break;
////    }
////    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == ITLI_AUTO_INJECTION_X19_ON) 
////    {
////      if (IOMsg.X19_AutoInjection != SIGNAL_ON) break;
////    }
////    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == ITLI_MOLD_OPENED_X18_ON) 
////    {
////      if (IOMsg.X18_MoldOpenComplete != SIGNAL_ON) break; 
////    }
////    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == ITLI_SAFTY_DOOR_X1A_ON) 
////    {
////      if (IOMsg.X1A_SaftyDoor != SIGNAL_ON) break;
////    }
////    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == ITLI_REJECT_X1B_ON) 
////    {
////      if (IOMsg.X1B_Reject != SIGNAL_ON) break;
////    }
////    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == ITLI_EMO_IMM_X1I_ON) 
////    {
////      if (IOMsg.X1I_EMOFromIMM != SIGNAL_ON) break;
////    }   
//    
////    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == ITLI_FULLAUTO_X1H_OFF) 
////    {
////      if (IOMsg.X1H_FullAuto != SIGNAL_OFF) break;
////    }
////    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == ITLI_AUTO_INJECTION_X19_OFF) 
////    {
////      if (IOMsg.X19_AutoInjection != SIGNAL_OFF) break;
////    }
////    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == ITLI_MOLD_OPENED_X18_OFF) 
////    {
////      if (IOMsg.X18_MoldOpenComplete != SIGNAL_OFF) break;
////    }
////    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == ITLI_SAFTY_DOOR_X1A_OFF) 
////    {
////      if (IOMsg.X1A_SaftyDoor != SIGNAL_OFF) break;
////    }
////    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == ITLI_REJECT_X1B_OFF) 
////    {
////      if (IOMsg.X1B_Reject != SIGNAL_OFF) break;
////    }
////    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == ITLI_EMO_IMM_X1I_OFF) 
////    {
////      if (IOMsg.X1I_EMOFromIMM != SIGNAL_OFF) break;
////    }
//           
////    if (SeqMsg.SeqArr[SeqMsg.SeqPos] == INPUT_CHUCK_OK_X16_ON) 
////    {
////      if (IOMsg.X16_MainChuckOk != SIGNAL_ON) SetSysTick(&SeqTempTick, TAKEOUT_CHECK);
////    }
////    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == INPUT_VACCUM_CHECK_X17_ON)
////    {
////      if (IOMsg.X17_MainVaccumOk != SIGNAL_ON) SetSysTick(&SeqTempTick, TAKEOUT_CHECK);
////    }
////    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == INPUT_SUB_CHUCK_OK_X1F_ON)
////    {
////      if (IOMsg.X1F_SubGripComplete != SIGNAL_ON) SetSysTick(&SeqTempTick, TAKEOUT_CHECK);
////    }
//
//    
//    /********************************************************  Motion  ********************************************************/
//  case MOTION_DELAY:
//    printf("MOTION \n\r");                  //Debug
//    
//    //    pMotionDelay = &(RobotCfg.MotionDelay.DownDelay);
//    //    printf("123123123123123123 : %d \n\r",pMotionDelay);
//    //    printf("123123123123123123 : %d \n\r",&(RobotCfg.MotionDelay.DownDelay));
//    //    pMotionDelay += (SeqMsg.SeqArr[SeqMsg.SeqPos] - 0x81);
//    //    printf("456456456456456456 : %d \n\r",pMotionDelay);
//    //    printf("456456456456456456 : %d \n\r",&(RobotCfg.MotionDelay.KickDelay));
//    //    test = (uint64_t)*(uint8_t *)pMotionDelay;
//    //    SetSysTick(&DelayTempTick, test*100);
//    
//    SetSysTick(&DelayTempTick, ((uint64_t)*(uint8_t*)(&(RobotCfg.MotionDelay.DownDelay)
//                                                      +(SeqMsg.SeqArr[SeqMsg.SeqPos]-SEQ_ENUM_DELAY)))*DELAY_TIME);  
//    //    SetSysTick(&DelayTempTick, 5000);
//    printf("SeqMsg.SeqArr[%d] : %d\n\r",SeqMsg.SeqPos,SeqMsg.SeqArr[SeqMsg.SeqPos]);    //Debug
//    printf("SeqMsg.SeqDispArr[%d] : %d\n\r",SeqMsg.SeqDispPos,SeqMsg.SeqDispArr[SeqMsg.SeqDispPos]);
//    printf("SeqMsg.SeqTime[%d] : %d\n\r",SeqMsg.SeqDispPos,SeqMsg.SeqTime[SeqMsg.SeqDispPos]);
//    SeqMsg.SeqDispPos++;
//    SeqLoopState = PROCESS_DONE;
//    break;
//    
//  case PROCESS_DONE:
//    
//    if (SeqMsg.SeqArr[SeqMsg.SeqPos] < SEQ_ENUM_DELAY) 
//    {
//      if(!ChkExpireSysTick(&SeqTempTick)) break;
//      printf("PROCESS_DONE \n\r\n\r");                  //Debug
//      SeqMsg.SeqPos++;
//      SeqLoopState = PROCESS_SEQ;
//    }
//    else if ((SeqMsg.SeqArr[SeqMsg.SeqPos] >= SEQ_ENUM_DELAY) && (SeqMsg.SeqArr[SeqMsg.SeqPos] != SEQ_ENUM_END))
//    {
//      if(!ChkExpireSysTick(&DelayTempTick)) break;
//      printf("PROCESS_DONE \n\r\n\r");                  //Debug
//      SeqMsg.SeqPos++;
//      SeqLoopState = PROCESS_SEQ;
//    }
//    else if (SeqMsg.SeqArr[SeqMsg.SeqPos] == SEQ_ENUM_END)
//    {
//      SeqLoopState = INIT_SEQ_LOOP;
//      fSeqEnd = 1;
//    }
//    //    PrintfSeqMsg(pSeqMsg);
//    break;
//    //    case SEND_SEQ:
//    //      if(!ChkExpireSysTick(&SeqTempTick)) break;
//    //      SeqLoopState = INIT_SEQ_LOOP;
//    //      break;
//  }
//}

//void StepSeq(void)
//{
//  
//}

//void PrintfSeqMsg(SEQ_MSG* pSeqMsgFunc)
//{  
//  // TEST
//  //  printf("SeqState   : 0x%X\n\r", pSeqMsgFunc->SeqState);
//  //  printf("SeqPos     : %5d\n\r", pSeqMsgFunc->SeqPos);
//  //  printf("SeqArr_  \n\r");
//  //  for(int i = 0; i < sizeof(pSeqMsgFunc->SeqArr); i++)
//  //  {
//  //    printf("%5d", pSeqMsgFunc->SeqArr[i]);
//  //    if(i%5 == 4)
//  //      printf("\n\r");
//  //  }
//  //  printf("SeqDispPos : %5d\n\r", pSeqMsgFunc->SeqDispPos);
//  //  printf("SeqDispArr_  \n\r");
//  //  for(int j = 0; j < sizeof(pSeqMsgFunc->SeqDispArr); j++)
//  //  {
//  //    printf("%5d", pSeqMsgFunc->SeqDispArr[j]);
//  //    if(j%5 == 4)
//  //      printf("\n\r");
//  //  }
//  //  printf("SeqTime_  \n\r");
//  //  for(int z = 0; z < sizeof(pSeqMsgFunc->SeqTime); z++)
//  //  {
//  //    printf("%5d", pSeqMsgFunc->SeqTime[z]);
//  //    if(z%5 == 4)
//  //      printf("\n\r");
//  //  }
//  //  printf("\n\r\n\r\n\r");
//}

void LoopAppSeq(void)
{
  //  static uint32_t cnt;
  //  static uint64_t TestTempTick;
  static enum
  {
    INIT = 0,
    CHK_SEQ_CMD,
    SEQ_AUTO,
    SEQ_CYCLE,
    SEQ_STEP,
    SEQ_MAKE_SEQ,    
  }SeqLoopState = INIT;
  
  switch(SeqLoopState)
  {
  case INIT:
    //    pSeqMsg = &SeqMsg;
    SeqLoopState = SEQ_MAKE_SEQ;
    break;
    
  case CHK_SEQ_CMD:
    break;
    
  case SEQ_MAKE_SEQ:
    //Make Seqence
    CmdReady();
//    MakeSequence();
//    DispSequence();
    SeqLoopState = SEQ_CYCLE;
    fSeqEnd = 0;
    break;
    
  case SEQ_CYCLE:
    //One Cycle
//    CycleSeq();
    //    if (SeqMsg.SeqArr[SeqMsg.SeqPos] == SEQ_END)
    //      SeqLoopState = SEQ_AUTO;
    if (fSeqEnd == 1)
      SeqLoopState = SEQ_AUTO;
    //    PrintfSeqMsg(pSeqMsg);
    //    SeqLoopState = INIT;
    break;
    
  case SEQ_STEP:
    //Step Run
    break;
    
  case SEQ_AUTO:
    //Auto Run
    break;   
  }
  
  
  //  case SEND_SEQ:                //Test
  //    if(!ChkExpireSysTick(&TempTick)) break;
  //    HAL_GPIO_TogglePin(LED3_GPIO_Port, LED3_Pin);
  //    PrintfSeqMsg(pSeqMsg);
  //    SeqLoopState = INIT;
  //    break;
  
  //    if ((RobotCfg.SC_Command & MASK_CMD_STATE_SEQ) == 0x00) break; 
  //    //SC_Command State
  //    if ((RobotCfg.SC_Command & MASK_CMD_STATE_SEQ) && CMD_STATE_STEP)
  //      SeqLoopState = SEQ_STEP;
  //    else if ((RobotCfg.SC_Command & MASK_CMD_STATE_SEQ) && CMD_STATE_CYCLE)
  //      SeqLoopState = SEQ_CYCLE;
  //    else if ((RobotCfg.SC_Command & MASK_CMD_STATE_SEQ) && CMD_STATE_AUTO)
  //      SeqLoopState = SEQ_AUTO;
  //    else if ((RobotCfg.SC_Command & MASK_CMD_STATE_SEQ) && CMD_STATE_MAKE_SEQ)
  //      SeqLoopState = SEQ_MAKE_SEQ;
  //    break;
  
  
  
  
  /***********************Debug test********************/
  //        pSeqMsg->SeqState++;
  //        pSeqMsg->SeqPos = 20;
  //        pSeqMsg->SeqDispPos = 30;
  //        for(int i = 0; i < sizeof(pSeqMsg->SeqArr); i++)
  //        {
  ////          pSeqMsg->SeqArr[i] = i*3;
  //          pSeqMsg->SeqArr[i] = i;
  //        }
  //        for(int i = 0; i < sizeof(pSeqMsg->SeqDispArr); i++)
  //        {
  //          pSeqMsg->SeqDispArr[i] = i*5;
  //        }
  //        for(int i = 0; i < sizeof(pSeqMsg->SeqTime); i++)
  //        {
  //          pSeqMsg->SeqTime[i] = i*10;
  //        }
  /*****************************************************/
  
  
  //	if(RobotCfg.SC_Command == CMD_STATE_STEP)
  //	{
  //
  //	}
  //	else if(RobotCfg.SC_Command == CMD_STATE_CYCLE)
  //	{
  //		
  //	}
  //	else if(RobotCfg.SC_Command == CMD_STATE_AUTO)
  //	{
  //
  //	}
  //	else if(RobotCfg.SC_Command == CMD_STATE_EMERGENCY)
  //	{
  //		
  //	}
}