/**
******************************************************************************
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __INTERFACE_H
#define __INTERFACE_H

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include "stm32f4xx_hal.h"
#include "timer.h"
/* Defines  ------------------------------------------------------------------*/
typedef uint8_t	IO;				//Digital Input / Ouptut
typedef uint8_t	ITL;				//Interlock

//#define CODE_PRINTF			
#define CODE_ERROR

#define CODE_ERROR_SENSOR_TIMEOUT
#define CODE_ERROR_FAIL_TAKE_OUT
#define CODE_ERROR_INTERLOCK
#define CODE_ERROR_PROCESS_TIME

//#define CODE_SWING_ALWAYS					//DEFAULT OFF
#define CODE_STEP_SEQ_ITL					//DEFAULT ON

#define CODE_BOOT_HOMING						//DEFAULT ON



//////////////#define DEBUG
//////////////#define RELEASE
//////////////#define ~~~~



#define ROBOT_TYPE_A 		0x00
#define ROBOT_TYPE_X 		0x01
#define ROBOT_TYPE_XC 		0x02
#define ROBOT_TYPE_TWIN		0x03

/**********IO********************/	 

/**************START OF STATE DEFINES**************/
	

/**************END OF STATE DEFINES**************/	

/**************MANUFACTURER SETTING**************/	

typedef struct _MANUFACTURER_SETTING{
  uint8_t ErrorTime;		//���� ���� ����
  uint8_t ItlFullAuto;
  uint8_t ItlSaftyDoor;
  uint8_t ItlAutoInjection;
  uint8_t ItlReject;
  uint8_t ProcessTime;	//
  uint8_t DelMoldData;
  uint8_t DelErrHistory;
  uint8_t DoorSignalChange; //DrSigCh
  uint8_t IMMType;	//IMMType
  uint8_t RotationState;	//IMMType
  
}MANUFACTURER_SETTING;

/**************MODE****************************/	
#define CONTROLMODE_MANUAL	1
#define CONTROLMODE_AUTO	2
#define CONTROLMODE_EMER	3
#define CONTROLMODE_STOP	4
#define CONTROLMODE_STEP	5
#define CONTROLMODE_CYCLE	6

/**************MODE****************************/	 
#define ROBOT_MAIN_MAIN							0x00
#define ROBOT_MAIN_SUB							0x01
#define ROBOT_MAIN_MS							0x02

#define NO_USE					0x00
#define USE						0x01

///******************************************/	

/******************************************/	
#define IO_M_UP		0
#define IO_M_DOWN		1


#define SIGNAL_ON			1
#define SIGNAL_OFF		0

/****************COMM* STATE*****************/	
#define CMD_STATE_MANUAL		0x01
#define CMD_STATE_STEP			0x02
#define CMD_STATE_CYCLE			0x04
#define CMD_STATE_AUTO			0x08
#define CMD_STATE_EMERGENCY	0x10
#define CMD_STATE_NORMAL		0x80
/****************COMM* STATE*****************/	
#define SIZE_SEQARR					99

typedef enum _MODE_ENUM
{
	MODE_INIT = 0x00,
	MODE_STANDBY = 0x01,	//normal
	MODE_SEQ = 0x04,
	
	MODE_MANUAL = 0x10,	
	MODE_STEP = 0x11,
	
	MODE_CYCLE_PAUSE = 0x20,
	MODE_CYCLE = 0x21,
	MODE_AUTO = 0x22,
	
	MODE_ERROR = 0x80,
	
}MODE_ENUM;
/* Exported types ------------------------------------------------------------*/

#define ERRCODE_EMG					0x60		//TP �����ġ//
#define ERRCODE_IMM_EMG				0x62		//���� ���//
#define ERRCODE_SWING_DUPLICATE	0x84		//���� ��ȣ �Ѵ� ����//	
#define ERRCODE_SUB_UP				0x86		//�ϰ� �� ���� �Ȳ���//
#define ERRCODE_MAIN_UP				0x87		//�ϰ� �� ���� �Ȳ���//
#define ERRCODE_SUB_UP_AIR			0x94		//��� �� ���� ������//
#define ERRCODE_MAIN_UP_AIR		0x96		//��� �� ���� ������//
#define ERRCODE_SWING				0x98//
#define ERRCODE_SWING_RETURN		0x99//

#define ERRCODE_VACCUM				0xA0		//���� Ÿ�Ӿƿ�	//�Ŵ��� ���� �ȵ�  ~~~~
#define ERRCODE_CHUCK				0xA1		//ô Ÿ�Ӿƿ�		//�Ŵ��� ���� �ȵ�  ~~~~
#define ERRCODE_RUNNER_PICK		0xA3		//���� Ÿ�Ӿƿ�	//�Ŵ��� ���� �ȵ�  ~~~~

#define ERRCODE_MO_SENSOR			0xC5		//INTERLOCK???????
#define ERRCODE_MO_SENS_MISS		0xCA		//�ϰ� �� �����Ϸ� ��ȣ ������.			~~~~
#define ERRCODE_MO_DOWN				0xD6		//�ϰ� ��, �����Ϸ� ����.(�Ŵ��� ���)  TP

#define ERRCODE_PROCESS_TIME		0xD7		



typedef enum _SEQ_ENUM
{
  OUTPUT_UPDOWN_Y20_ON = 0x01,       //0x01
  OUTPUT_KICK_Y21_ON,             
  OUTPUT_CHUCK_Y22_ON,                                 
  OUTPUT_SWING_Y23_ON,        
  OUTPUT_SWING_RETURN_Y2F_ON,
  OUTPUT_ROTATE_CHUCK_Y24_ON,      
  OUTPUT_VACCUM_Y25_ON,                           
  OUTPUT_NIPPER_Y26_ON,      
  OUTPUT_SUB_UPDOWN_Y2D_ON,
  OUTPUT_SUB_KICK_Y2E_ON,
  OUTPUT_SUB_CHUCK_Y27_ON,                        
  OUTPUT_ALARM_Y28_ON,   
  OUTPUT_MAIN_POWER_Y2G_ON,
  
  ITLO_CYCLE_START_Y29_ON,         //0x0E
  ITLO_MOLD_OPENCLOSE_Y2A_ON,
  ITLO_EJECTOR_Y2B_ON,
  ITLO_CONVEYOR_Y2C_ON,
  ITLO_BUZZER_Y28_ON,
  
  INPUT_UP_COMPLETE_X11_ON,   //0x13
  INPUT_CHUCK_OK_X16_ON,                          //0x14
  INPUT_SWING_DONE_X14_ON,
  INPUT_SWING_RETURN_DONE_X15_ON,
  INPUT_VACCUM_CHECK_X17_ON,                      //0x17
  INPUT_SUB_UP_COMPLETE_X1G_ON,
  INPUT_SUB_CHUCK_OK_X1F_ON,                      //0x19
  
  ITLI_FULLAUTO_X1H_ON,           //0x1A
  ITLI_AUTO_INJECTION_X19_ON,
  ITLI_MOLD_OPENED_X18_ON,
  ITLI_SAFTY_DOOR_X1A_ON,
  ITLI_REJECT_X1B_ON,
  ITLI_EMO_IMM_X1I_ON,
  
  
  
  OUTPUT_UPDOWN_Y20_OFF = 0x41,     //0x41
  OUTPUT_KICK_Y21_OFF,
  OUTPUT_CHUCK_Y22_OFF,
  OUTPUT_SWING_Y23_OFF,
  OUTPUT_SWING_RETURN_Y2F_OFF,
  OUTPUT_ROTATE_CHUCK_Y24_OFF,
  OUTPUT_VACCUM_Y25_OFF,
  OUTPUT_NIPPER_Y26_OFF, 
  OUTPUT_SUB_UPDOWN_Y2D_OFF,
  OUTPUT_SUB_KICK_Y2E_OFF,
  OUTPUT_SUB_CHUCK_Y27_OFF,
  OUTPUT_ALARM_Y28_OFF,
  OUTPUT_MAIN_POWER_Y2G_OFF,
  
  ITLO_CYCLE_START_Y29_OFF,       //0x4E
  ITLO_MOLD_OPENCLOSE_Y2A_OFF,
  ITLO_EJECTOR_Y2B_OFF,
  ITLO_CONVEYOR_Y2C_OFF,
  ITLO_BUZZER_Y28_OFF,
  
  INPUT_UP_COMPLETE_X11_OFF, //0x53
  INPUT_CHUCK_OK_X16_OFF,
  INPUT_SWING_DONE_X14_OFF,
  INPUT_SWING_RETURN_DONE_X15_OFF,
  INPUT_VACCUM_CHECK_X17_OFF,
  INPUT_SUB_UP_COMPLETE_X1G_OFF,
  INPUT_SUB_CHUCK_OK_X1F_OFF,
  
  ITLI_FULLAUTO_X1H_OFF,  //0x5A
  ITLI_AUTO_INJECTION_X19_OFF,
  ITLI_MOLD_OPENED_X18_OFF,
  ITLI_SAFTY_DOOR_X1A_OFF,
  ITLI_REJECT_X1B_OFF,
  ITLI_EMO_IMM_X1I_OFF,
  
  DELAY_DOWN = 0x81,
  DELAY_KICK,
  DELAY_EJECTOR,
  DELAY_CHUCK,
  DELAY_KICK_RETURN,
  DELAY_UP,
  DELAY_SWING,
  DELAY_DOWN2ND,
  DELAY_OPEN,
  DELAY_UP2ND,
  DELAY_CHUCK_ROTATE_RETURN,
  DELAY_SWING_RETURN,
  DELAY_NIPPER,
  DELAY_CONVEYOR,
  DELAY_USER1,
  DELAY_USER2,
  DELAY_USER3,
  DELAY_USER4,
  DELAY_USER5,
  DELAY_USER6,
  DELAY_USER7,
  DELAY_USER8,
  
  ITLO_CYCLE_START_Y29_ON_FIRST = 0xC0,
  ITLO_MOLD_OPENCLOSE_Y2A_ON_FIRST,
  
  SEQ_END = 0xFF,        
}SEQ_ENUM;
typedef struct _DELAY_MSG{
	uint8_t	DelayDown;
	uint8_t	DelayKick;
	uint8_t	DelayEjector;
	uint8_t	DelayChuck;
	uint8_t	DelayKickReturn;
	uint8_t	DelayUp;
	uint8_t	DelaySwing;
	uint8_t	DelayDown2nd;
	uint8_t	DelayOpen;
	uint8_t	DelayUp2nd;
	uint8_t	DelayChuckRotateReturn;
	uint8_t	DelaySwingReturn;
	uint8_t	DelayNipper;
	uint8_t	DelayConveyor;
	uint8_t UserDelay1;
	uint8_t UserDelay2;
	uint8_t UserDelay3;
	uint8_t UserDelay4;
	uint8_t UserDelay5;
	uint8_t UserDelay6;
	uint8_t UserDelay7;
	uint8_t UserDelay8;
}DELAY_MSG;

typedef struct _IO_MSG{
  IO Y20_Down;
  IO Y21_Kick;
  IO Y22_Chuck;
  IO Y23_Swing;
  IO Y2F_SwingReturn;
  IO Y24_ChuckRotation;
  IO Y25_Vaccum;
  IO Y26_Nipper;
  IO Y2D_SubUp;
  IO Y2E_SubKick;
  IO Y27_SubGrip;
  IO Y28_Alarm;
  IO Y2G_MainPower;
  
  ITL Y29_CycleStart;
  ITL Y2A_MoldOpenClose;
  ITL Y2B_Ejector;
  ITL Y2C_Conveyor;
  ITL Y28_Buzzer;
  
  IO X11_MainArmUpComplete;
  IO X16_MainChuckOk;
  IO X14_SwingOk;
  IO X15_SwingReturnOk;
  IO X17_MainVaccumOk;
  IO X1G_SubUpComplete;
  IO X1F_SubGripComplete;       
  
  ITL X1H_FullAuto;      
  ITL X19_AutoInjection;
  ITL X18_MoldOpenComplete;
  ITL X1A_SaftyDoor;
  ITL X1B_Reject;
  ITL X1I_EMOFromIMM;
}IO_MSG;


typedef struct _COUNT_MSG{
  uint32_t TotalCnt;
  uint32_t RejectCnt;
  uint32_t DetectFail;
}COUNT_MSG;


/**************************************************************************/
#define IO_UPDATE_READY			0x00
#define IO_UPDATE_PROCESS		0x01

#define IO_EMERGENCY_INACTIVE	0x00
#define IO_EMERGENCY_ACTIVE	0x01

#define EMC_CHK_MASK				0x0000//0xA000



#define STEP_ING			0x00
#define STEP_END			0x01

//#define STEP_CONTINUE		0x00
//#define STEP_STANDBY			0x01
//#define HOMING_NEED		0x01
//#define HOMING_NOT		0x00

#define HOMING_DONE		0x01
#define HOMING_ING		0x00

#define CONVEYOR_START		0x01
#define CONVEYOR_RUN			0x02
#define CONVEYOR_STOP		0x00

#define ROBOT_MAIN_MAIN							0x00
#define ROBOT_MAIN_SUB							0x01
#define ROBOT_MAIN_MS							0x02
//#define ERRCODE_EMG					0x60		//TP �����ġ
//#define ERRCODE_IMM_EMG				0x62		//���� ���
//#define ERRCODE_SWING_DUPLICATE	0x84		//���� ��ȣ �Ѵ� ����
//#define ERRCODE_SUB_UP				0x86		//�ϰ� �� ���� �Ȳ���
//#define ERRCODE_MAIN_UP				0x87		//�ϰ� �� ���� �Ȳ���
//#define ERRCODE_SUB_UP_AIR			0x94		//��� �� ���� ������
//#define ERRCODE_MAIN_UP_AIR		0x96		//��� �� ���� ������
//#define ERRCODE_SWING				0x98
//#define ERRCODE_SWING_RETURN		0x99
//#define ERRCODE_VACCUM				0xA0		//���� Ÿ�Ӿƿ�
//#define ERRCODE_CHUCK				0xA1		//ô Ÿ�Ӿƿ�
//#define ERRCODE_RUNNER_PICK		0xA3		//���� Ÿ�Ӿƿ�
//
//#define ERRCODE_MO_SENSOR			0xC5		//???????
//#define ERRCODE_MO_SENS_MISS		0xCA		//�ϰ� �� �����Ϸ� ��ȣ ������.
//#define ERRCODE_MO_DOWN				0xD6		//�ϰ� ��, �����Ϸ� ����.(�Ŵ��� ���)
//#define ERRCODE_PROCESS_TIME		0xD7		


//#define ERROR_OCCUR				0x01
//#define ERROR_NONE				0x00


#define ERROR_STATE_NONE		0x00
#define ERROR_STATE_OCCUR		0x01 //SC, TP //ErrCode
#define ERROR_STATE_CLEAR		0x02 //TP Clear		//BUZZER OFF
#define ERROR_STATE_ACCEPT		0x03 //SC OK --> TP 0x00	//MODE CHANGE
#define ERROR_STATE_RESET		0x04


//#define

typedef struct _CONTROL_MSG{
//  uint8_t IO_Update;	//when comm, emergency, Auto Sequence
  MODE_ENUM SCMode;
//  MODE_ENUM	pastSCMode;
  
  uint8_t SeqPos;						
  SEQ_ENUM SeqArr[SIZE_SEQARR];		//IO  Sequence
  DELAY_MSG DelayMsg;
  
  COUNT_MSG Count;
  
  uint8_t fHoming;
  uint8_t fConveyor;
  uint8_t TimerConveyor;
//  uint8_t fHomingDone;
  
  uint8_t OutsideWait;		//0 : default 1: Outwait
  uint8_t MainArmDownPos; //0:�̵���, 1: ������
  uint8_t SubArmDownPos; 	//0: ������ , 1: �̵���
  uint8_t ErrorTime;
  uint8_t ProcessTime;
//  uint8_t Detection;
  uint8_t SaftyDoor;
  uint8_t MotionArm;
  
  uint8_t RealTimerRemain;	//Delays Real Time  
  
  uint8_t StepSeq;
  uint8_t StepStandby;

  uint8_t ErrState;
  uint8_t ErrCode;
  uint8_t fErrSaftyDoor;
  
  uint8_t TpEmr;
  
  uint8_t DoHoming;
#ifdef CODE_ERROR_SENSOR_TIMEOUT
//  uint8_t PastInputSeqPos;		//0x00 : None, 
  uint8_t InputTimeState;
#endif
  
  
  uint64_t ProcessTimeTick;
  uint64_t InTimeOutTick;
  
  uint8_t Buzzer;
 
  MANUFACTURER_SETTING Setting;
  
}CONTROL_MSG;
#define IN_TIME_STATE_STANDBY		0x00
#define IN_TIME_STATE_START		0x01
#define IN_TIME_STATE_ING			0x02
#define IN_TIME_STATE_END			0x03

#define HOLD_ERROR_STATE_NONE		0x00
#define HOLD_ERROR_STATE_OCCUR	0x01
#define HOLD_ERROR_STATE_SILENT_BUZZER	0x02
#define HOLD_ERROR_STATE_GO_NONE	0x03



/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */


#endif /* __INTERFACE_H */

/******END OF FILE****/

