/* 
 * File:   __APPIO_H.h
 * Author: Jay Park
 *
 * Created on 2016년 11월 14일 (Mon), PM 3:33
 */

#ifndef __APPIO_H
#define	__APPIO_H

#ifdef	__cplusplus
extern "C" {
#endif

#include "interface.h"
#include "main.h"   
	
//CONTROL_MSG ControlMsg;	

//#ifdef
//#elif
//#else
//#endif
   
#define TIME_IO 		10
#define TIME_INIT		1000
#define TIMEOUT_INIT	5000

#define TIME_SEQ		20

#define TIME_HOMING_KICK		300
#define TIME_HOMING_UP			300
#define TIME_CHUCK_RR			300

/* FatFs includes component */
#define BIT0		(uint16_t)0x0001
#define BIT1		(uint16_t)0x0002
#define BIT2		(uint16_t)0x0004
#define BIT3		(uint16_t)0x0008
#define BIT4		(uint16_t)0x0010
#define BIT5		(uint16_t)0x0020
#define BIT6		(uint16_t)0x0040
#define BIT7		(uint16_t)0x0080
#define BIT8		(uint16_t)0x0100
#define BIT9		(uint16_t)0x0200
#define BIT10		(uint16_t)0x0400
#define BIT11		(uint16_t)0x0800
#define BIT12		(uint16_t)0x1000
#define BIT13		(uint16_t)0x2000
#define BIT14		(uint16_t)0x4000
#define BIT15		(uint16_t)0x8000


	

#define ADDR_X000X015         	((uint32_t)0x6C300000)
#define ADDR_Y000Y015         	((uint32_t)0x6C240000)

#define Y20_UPDOWN			BIT0
#define Y21_KICK           BIT1
#define Y22_CHUCK          BIT2
#define Y23_SWING          BIT3
#define Y24_ROTATION       BIT4
#define Y25_VACCUM         BIT5
#define Y26_NIPPER         BIT6
#define Y27_SUB_GRIP       BIT7
#define Y28_ALARM          BIT8
#define Y2F_SWING_RETURN   BIT9
#define Y2D_SUB_UPDOWN     BIT10
#define Y2E_SUB_KICK       BIT11
#define Y28_BUZZER         BIT12
#define Y2B_EJECTOR        BIT13
#define Y29_CYCLE_START    BIT14
#define Y2A_MOLD_ENABLE		BIT15			//Lock//MOLD_OPEN_CLOSE
#define Y2C_CONVEYOR
	

#define	X1G_SUB_UPDOWN				BIT0
#define	X11_UPDOWN              BIT1
#define	X16_CHUCK               BIT2
#define	X17_VACCUM              BIT3
#define	X1F_SUB_GRIP            BIT4
#define	XX1_RESERVED            BIT5
#define	X14_SWING               BIT6
#define	X15_SWING_RETURN        BIT7
#define	X18_MOLD_OPEN_COMPLETE  BIT8
#define	X1A_DOOR_OPEN				BIT9				//SAFTY DOOR
#define	X1H_FULL_AUTO           BIT10
#define	X1B_REJECT					BIT11				//Abnormal product
#define	X19_AUTO_INJECTION      BIT12
#define	X1I_IMM_EMR             BIT13
#define	XX2_RESERVED            BIT14
#define	X20_TP_EMR              BIT15

#define I_TRUE				0x01
#define I_FALSE			0x00
	
#define D_TRUE				0x01
#define D_FALSE			0x00
#define D_ERROR			0x02
	
#define SEQ_INPUT			0x00
#define SEQ_OUTPUT		0x01
#define SEQ_DELAY			0x02
#define SEQ_ELSE			0x03
	
#define LOOP_RUN		0x00		//LoopInit
#define LOOP_DONE		0x01
	
#define REFRESH_ON		0x01
#define REFRESH_0FF		0x00
	
#define SEQ_MODE_CHANGED		0x01
#define SEQ_MODE_STAY			0x00

  
#define MOLD_INIT		0x00
#define MOLD_CLOSE 	0x01
#define MOLD_OPEN 	0x02

#define FIRST_START 		0x00
#define SECOND_CYCLE 	0x01

  
void LoopAppIO(void);
void LoopEMO(void);
void LoopRunSeq(void);
void SetOutput(SEQ_ENUM Cmd, uint8_t Refresh);
#ifdef	__cplusplus
}
#endif

#endif	/* __APPIO_H */

