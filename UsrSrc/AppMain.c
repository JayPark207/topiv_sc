#include "AppMain.h"

extern CONTROL_MSG ControlMsg;
//extern ROBOT_CONFIG	RobotCfg;
//extern SEQ_MSG SeqMsg;
extern IO_MSG IOMsg;
extern UART_HandleTypeDef huart1;

void LoopAppLED(void)
{
  static uint64_t TempLEDTick = 0;
  
  static enum
  {
	 INIT,
	 LED_TOGGLE,
  }LedLoopState = INIT;
  switch(LedLoopState)
  {
  case INIT:
	 
	 SetSysTick(&TempLEDTick, 50);
	 LedLoopState = LED_TOGGLE;
	 break;
	 
  case LED_TOGGLE:
	 if(!ChkExpireSysTick(&TempLEDTick)) break;
	 HAL_GPIO_TogglePin(LED0_GPIO_Port, LED0_Pin);
	 SetSysTick(&TempLEDTick, 500);
	 break;
  }
  //	HAL_GPIO_TogglePin(LED0_GPIO_Port, LED0_Pin);
}


void LoopAppEMO(void)
{
  static uint64_t TempEMOTick = 0;
  static uint64_t BuzzerTick = 0;
  static enum
  {
	 EMO_INIT = 0,
	 
	 CHECK_EMO,
	 
	 RUN_EMO,
	 
  }EMOLoopState = EMO_INIT; 
  switch(EMOLoopState)
  {
  case EMO_INIT:
	 SetSysTick(&TempEMOTick, TIME_EMO);
	 EMOLoopState = CHECK_EMO;
	 break;
	 
  case CHECK_EMO:
	 if(!ChkExpireSysTick(&TempEMOTick)) break;
	 SetSysTick(&TempEMOTick, TIME_EMO);
	 
	 
	 if(ControlMsg.TpEmr == SIGNAL_OFF)
	 {
		ControlMsg.SCMode = MODE_ERROR;
		ControlMsg.ErrState = ERROR_STATE_OCCUR;
		ControlMsg.ErrCode = ERRCODE_EMG;
	 }
	 else if(IOMsg.X1I_EMOFromIMM == SIGNAL_OFF)
	 {
		ControlMsg.SCMode = MODE_ERROR;
		ControlMsg.ErrState = ERROR_STATE_OCCUR;
		ControlMsg.ErrCode = ERRCODE_IMM_EMG;
	 }
	 break;
  }
  
  static enum
  {
	 BUZZER_INIT = 0,
	 
	 CHECK_ERROR,
	 
	 RUN_BUZZER_ON,
	 RUN_BUZZER_OFF,
	 
	 
  }BuzzerLoopState = BUZZER_INIT; 
  switch(BuzzerLoopState)
  {
  case BUZZER_INIT:
//	 IOMsg.Y28_Buzzer = SIGNAL_OFF;
//	 IOMsg.Y28_Alarm = SIGNAL_OFF;
	 
	 SetOutput(ITLO_BUZZER_Y28_OFF, REFRESH_ON);
	 SetOutput(OUTPUT_ALARM_Y28_OFF, REFRESH_ON);
	 BuzzerLoopState = CHECK_ERROR;
	 
	 break;
	 
  case CHECK_ERROR:
	 if(ControlMsg.ErrState == ERROR_STATE_OCCUR)
	 {
		//		SetSysTick(&BuzzerTick, TIME_BUZZER_ON);
		if(ControlMsg.Buzzer == USE)
		{
		  SetOutput(ITLO_BUZZER_Y28_ON, REFRESH_ON);
		  SetOutput(OUTPUT_ALARM_Y28_ON, REFRESH_ON);
		}
//		IOMsg.Y28_Buzzer = SIGNAL_ON;
//		IOMsg.Y28_Alarm = SIGNAL_ON;
		BuzzerLoopState = RUN_BUZZER_ON;
	 }
	 break;
	 
  case RUN_BUZZER_ON:
	 if(ControlMsg.ErrState != ERROR_STATE_OCCUR)
	 {
//		IOMsg.Y28_Buzzer = SIGNAL_OFF;
//		IOMsg.Y28_Alarm = SIGNAL_OFF;
		SetOutput(ITLO_BUZZER_Y28_OFF, REFRESH_ON);
		SetOutput(OUTPUT_ALARM_Y28_OFF, REFRESH_ON);
		BuzzerLoopState = CHECK_ERROR;
	 }
//	 if(!ChkExpireSysTick(&BuzzerTick)) break;
//	 SetSysTick(&BuzzerTick, TIME_BUZZER_OFF);
//	 BuzzerLoopState = RUN_BUZZER_OFF;
	 
	 break;
  case RUN_BUZZER_OFF:
	 if(ControlMsg.ErrState != ERROR_STATE_OCCUR)
	 {
//		IOMsg.Y28_Buzzer = SIGNAL_OFF;
//		IOMsg.Y28_Alarm = SIGNAL_OFF;
		SetOutput(ITLO_BUZZER_Y28_OFF, REFRESH_ON);
		SetOutput(OUTPUT_ALARM_Y28_OFF, REFRESH_ON);
		BuzzerLoopState = CHECK_ERROR;
	 }
	 if(!ChkExpireSysTick(&BuzzerTick)) break;
	 SetSysTick(&BuzzerTick, TIME_BUZZER_OFF);
	 BuzzerLoopState = CHECK_ERROR;
	 
	 break;
  }
}



