/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __APPMAIN_H
#define __APPMAIN_H
#ifdef __cplusplus
 extern "C" {
#endif
/* Includes -------------------------------------------------------------------*/

#include "interface.h"
#include "main.h"
#include "timer.h"
#include "Queue.h"

/* typedef --------------------------------------------------------------------*/


/* define ---------------------------------------------------------------------*/
#define TIME_EMO		10//5
#define TIME_BUZZER_ON		2000
#define TIME_BUZZER_OFF		1000
	
#define REFRESH_ON		0x01
#define REFRESH_0FF		0x00
/* macro ----------------------------------------------------------------------*/

/* function prototypes --------------------------------------------------------*/
extern void LoopAppMain(void);

/* variables ------------------------------------------------------------------*/

/* ----------------------------------------------------------------------------*/
void LoopAppLED(void);
void LoopAppEMO(void);
void SetOutput(SEQ_ENUM Cmd, uint8_t Refresh);

#ifdef __cplusplus
}
#endif
#endif /*__APPMAIN_H */