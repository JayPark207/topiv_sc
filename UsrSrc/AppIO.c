#include "AppIO.h"
#include "stm32f4xx_hal.h"
uint16_t Output;
uint16_t Input;
IO_MSG IOMsg;
uint8_t Conveyor;

uint8_t fHomingSEQ;
uint8_t AutoCycleCount;
uint8_t MoldOpenCloseCheck;
uint8_t AutoInjectionTemp;

CONTROL_MSG ControlMsg;

SEQ_ENUM pastDelaySeq = (SEQ_ENUM)0xEE;
#ifdef CODE_ERROR
void ErrorInputTimeOutProcess(SEQ_ENUM Seq)
{
  if(!(Seq == INPUT_UP_COMPLETE_X11_ON || Seq == INPUT_SUB_UP_COMPLETE_X1G_ON ||
		 Seq == INPUT_UP_COMPLETE_X11_OFF || Seq == INPUT_SUB_UP_COMPLETE_X1G_OFF ||
			Seq == INPUT_SWING_DONE_X14_ON || Seq == INPUT_SWING_RETURN_DONE_X15_ON ||
			  Seq == INPUT_VACCUM_CHECK_X17_ON || Seq == INPUT_CHUCK_OK_X16_ON ||
				 Seq == INPUT_SUB_CHUCK_OK_X1F_ON)) return;
  //  if((ControlMsg.SCMode != MODE_AUTO) && ((Seq == INPUT_VACCUM_CHECK_X17_ON) || (Seq == INPUT_CHUCK_OK_X16_ON) ||
  //        (Seq == INPUT_SUB_CHUCK_OK_X1F_ON) )) return;
  
  //  if(!(Seq == INPUT_UP_COMPLETE_X11_ON || Seq == INPUT_SUB_UP_COMPLETE_X1G_ON ||
  //		 Seq == INPUT_UP_COMPLETE_X11_OFF || Seq == INPUT_SUB_UP_COMPLETE_X1G_OFF ||
  //		 Seq == INPUT_SWING_DONE_X14_ON)) return;
  //  if(!(Seq == INPUT_SWING_RETURN_DONE_X15_ON ||
  //		 Seq == INPUT_VACCUM_CHECK_X17_ON || Seq == INPUT_CHUCK_OK_X16_ON ||
  //		 Seq == INPUT_SUB_CHUCK_OK_X1F_ON)) return;
  
  //  ControlMsg.SeqPos = 0;
  //  ControlMsg.InputTimeState = IN_TIME_STATE_STANDBY;
  
  if(ControlMsg.ErrState == ERROR_STATE_NONE) ControlMsg.ErrState = ERROR_STATE_OCCUR;
  else return;
  
  ControlMsg.SCMode = MODE_ERROR;
  
  //InputSeq = 0x13~19, 0x53~0x59
  if(Seq == INPUT_UP_COMPLETE_X11_ON)
  {
	 ControlMsg.ErrCode = ERRCODE_MAIN_UP_AIR;				//0x96
  }
  else if(Seq == INPUT_SUB_UP_COMPLETE_X1G_ON)
  {
	 ControlMsg.ErrCode = ERRCODE_SUB_UP_AIR;	//0x94
  }
  else if(Seq == INPUT_UP_COMPLETE_X11_OFF)
  {
	 ControlMsg.ErrCode = ERRCODE_MAIN_UP;			//0x87
  }
  else if(Seq == INPUT_SUB_UP_COMPLETE_X1G_OFF)
  {
	 ControlMsg.ErrCode = ERRCODE_SUB_UP;		//0x86
  }
  else if(Seq == INPUT_SWING_DONE_X14_ON)
  {
	 ControlMsg.ErrCode = ERRCODE_SWING;				//0x98
  }
  else if(Seq == INPUT_SWING_RETURN_DONE_X15_ON)
  {
	 ControlMsg.ErrCode = ERRCODE_SWING_RETURN;//0x99
  }
  
  else if(Seq == INPUT_VACCUM_CHECK_X17_ON)
  {
	 if(ControlMsg.SaftyDoor == USE) ControlMsg.fErrSaftyDoor = HOLD_ERROR_STATE_OCCUR;
	 ControlMsg.ErrCode = ERRCODE_VACCUM;			//0xA0
	 ControlMsg.Count.DetectFail++;
	 if(ControlMsg.MotionArm == ROBOT_MAIN_MS)
	 {
		SetOutput(OUTPUT_UPDOWN_Y20_OFF, REFRESH_ON);
		SetOutput(OUTPUT_SUB_UPDOWN_Y2D_OFF, REFRESH_ON);
	 }
	 else if(ControlMsg.MotionArm == ROBOT_MAIN_MAIN)
	 {
		SetOutput(OUTPUT_UPDOWN_Y20_OFF, REFRESH_ON);
	 }
	 else if(ControlMsg.MotionArm == ROBOT_MAIN_SUB)
	 {
		SetOutput(OUTPUT_SUB_UPDOWN_Y2D_OFF, REFRESH_ON);
	 }
  }
  else if(Seq == INPUT_CHUCK_OK_X16_ON)
  {
	 if(ControlMsg.SaftyDoor == USE) ControlMsg.fErrSaftyDoor = HOLD_ERROR_STATE_OCCUR;
	 ControlMsg.ErrCode = ERRCODE_CHUCK;					//0xA1
	 ControlMsg.Count.DetectFail++;
	 if(ControlMsg.MotionArm == ROBOT_MAIN_MS)
	 {
		SetOutput(OUTPUT_UPDOWN_Y20_OFF, REFRESH_ON);
		SetOutput(OUTPUT_SUB_UPDOWN_Y2D_OFF, REFRESH_ON);
	 }
	 else if(ControlMsg.MotionArm == ROBOT_MAIN_MAIN)
	 {
		SetOutput(OUTPUT_UPDOWN_Y20_OFF, REFRESH_ON);
	 }
	 else if(ControlMsg.MotionArm == ROBOT_MAIN_SUB)
	 {
		SetOutput(OUTPUT_SUB_UPDOWN_Y2D_OFF, REFRESH_ON);
	 }
  }
  else if(Seq == INPUT_SUB_CHUCK_OK_X1F_ON)
  {
	 if(ControlMsg.SaftyDoor == USE) ControlMsg.fErrSaftyDoor = HOLD_ERROR_STATE_OCCUR;
	 ControlMsg.ErrCode = ERRCODE_RUNNER_PICK;		//0xA3
	 ControlMsg.Count.DetectFail++;
	 if(ControlMsg.MotionArm == ROBOT_MAIN_MS)
	 {
		SetOutput(OUTPUT_UPDOWN_Y20_OFF, REFRESH_ON);
		SetOutput(OUTPUT_SUB_UPDOWN_Y2D_OFF, REFRESH_ON);
	 }
	 else if(ControlMsg.MotionArm == ROBOT_MAIN_MAIN)
	 {
		SetOutput(OUTPUT_UPDOWN_Y20_OFF, REFRESH_ON);
	 }
	 else if(ControlMsg.MotionArm == ROBOT_MAIN_SUB)
	 {
		SetOutput(OUTPUT_SUB_UPDOWN_Y2D_OFF, REFRESH_ON);
	 }
  }
  else
  {
	 //	 ControlMsg.Count.DetectFail++;
	 //		ControlMsg.ErrState = ERROR_STATE_OCCUR;
	 //	 if(ControlMsg.SaftyDoor == USE) ControlMsg.fErrSaftyDoor = HOLD_ERROR_STATE_OCCUR;
  }
}
void ErrorProcess(uint8_t ErrorCode)
{
  if(ControlMsg.ErrState == ERROR_STATE_NONE) ControlMsg.ErrState = ERROR_STATE_OCCUR;
  else return;
  ControlMsg.ErrCode = ErrorCode;	//ProcessTimeOut = 0xD7 //Swing Dual = 0x84
  //	printf("ERR Process : %x\n\r",ControlMsg.ErrCode);
  //	ControlMsg.SCMode = MODE_MANUAL;
}
#endif
//void ErrorClear(void)
//{
//	ControlMsg.ErrState = ERROR_NONE;
//}

void EncodingOutput(IO_MSG* pIOMsg)
{
  Output = pIOMsg->Y20_Down;
  Output |= (pIOMsg->Y21_Kick) << 1;
  Output |= (pIOMsg->Y22_Chuck) << 2;
  Output |= (pIOMsg->Y23_Swing) << 3;
  
  Output |= (pIOMsg->Y24_ChuckRotation) << 4;
  Output |= (pIOMsg->Y25_Vaccum) << 5;
  Output |= (pIOMsg->Y26_Nipper) << 6;
  Output |= (pIOMsg->Y27_SubGrip) << 7;
  
  Output |= (pIOMsg->Y28_Buzzer) << 8;		//Y28_Buzzer
  Output |= (pIOMsg->Y2F_SwingReturn) << 9;
  Output |= (pIOMsg->Y2D_SubUp) << 10;
  Output |= (pIOMsg->Y2E_SubKick) << 11;
  
  Output |= (pIOMsg->Y28_Buzzer) << 12;
  Output |= (pIOMsg->Y2B_Ejector) << 13;
  Output |= (pIOMsg->Y29_CycleStart) << 14;
  Output |= (pIOMsg->Y2A_MoldOpenClose) << 15;
  
  Conveyor = pIOMsg->Y2C_Conveyor;
  
}
void DecodingInput(IO_MSG* pIOMsg, uint8_t* pTPEmr)
{
  pIOMsg->X1G_SubUpComplete = (Input & X1G_SUB_UPDOWN);
  pIOMsg->X11_MainArmUpComplete = (Input & X11_UPDOWN) >> 1;
  pIOMsg->X16_MainChuckOk = (Input & X16_CHUCK) >> 2;
  pIOMsg->X17_MainVaccumOk = (Input & X17_VACCUM) >> 3;
  
  pIOMsg->X1F_SubGripComplete = (Input & X1F_SUB_GRIP) >> 4;
  //	pIOMsg->X11_MainArmUpComplete = (Input & XX1_RESERVED) >> 5;
  pIOMsg->X14_SwingOk = (Input & X14_SWING) >> 6;
  pIOMsg->X15_SwingReturnOk = (Input & X15_SWING_RETURN) >> 7;
  
  pIOMsg->X18_MoldOpenComplete = (Input & X18_MOLD_OPEN_COMPLETE) >> 8;
  pIOMsg->X1A_SaftyDoor = (Input & X1A_DOOR_OPEN) >> 9;
  pIOMsg->X1H_FullAuto = (Input & X1H_FULL_AUTO) >> 10;
  pIOMsg->X1B_Reject = (Input & X1B_REJECT) >> 11;
  
  pIOMsg->X19_AutoInjection = (Input & X19_AUTO_INJECTION) >> 12;
  pIOMsg->X1I_EMOFromIMM = (Input & X1I_IMM_EMR) >> 13;
  //	pIOMsg->X11_MainArmUpComplete = (Input & XX2_RESERVED) >> 14;
  *pTPEmr = (Input & X20_TP_EMR) >> 15;		//Need Correct
}

void ReadInput(uint16_t* pInput)
{
  *pInput = ~(uint16_t) *(__IO uint16_t*)(ADDR_X000X015);
}

void WriteOutput(uint16_t* pOutput)
{
  *(__IO uint16_t*) (ADDR_Y000Y015) = *(pOutput);//(uint16_t*)
  if(Conveyor) HAL_GPIO_WritePin(CONVEYOR_OUT_GPIO_Port, CONVEYOR_OUT_Pin, GPIO_PIN_SET);
  else HAL_GPIO_WritePin(CONVEYOR_OUT_GPIO_Port, CONVEYOR_OUT_Pin, GPIO_PIN_RESET);
}

void MakeInitState(IO_MSG* pIOMsg)
{
  ReadInput(&Input);
  DecodingInput(&IOMsg, &ControlMsg.TpEmr);
  
//  if(IOMsg.X11_MainArmUpComplete == SIGNAL_ON) IOMsg.Y20_Down = SIGNAL_OFF;
//  if(IOMsg.X1G_SubUpComplete == SIGNAL_ON) IOMsg.Y2D_SubUp = SIGNAL_OFF;
  
  if(IOMsg.X14_SwingOk == SIGNAL_ON) IOMsg.Y23_Swing = SIGNAL_ON;
  if(IOMsg.X15_SwingReturnOk == SIGNAL_ON) IOMsg.Y2F_SwingReturn = SIGNAL_ON;
  
//  if(IOMsg.X16_MainChuckOk == SIGNAL_ON) IOMsg.Y22_Chuck = SIGNAL_ON;
//  if(IOMsg.X17_MainVaccumOk == SIGNAL_ON) IOMsg.Y25_Vaccum = SIGNAL_ON;
//  if(IOMsg.X1F_SubGripComplete == SIGNAL_ON) IOMsg.Y27_SubGrip = SIGNAL_ON;
  
  EncodingOutput(&IOMsg);
  WriteOutput(&Output);
}

void SetOutput(SEQ_ENUM Cmd, uint8_t Refresh)
{
  //RANGE  0x01~0x1F, 0x41~0x5F
  uint8_t temp;
  uint8_t IO_Distinguish = 0; //1: ON, 0:OFF
  
  if(Cmd < OUTPUT_UPDOWN_Y20_OFF) IO_Distinguish = 1;	//0x41

  temp = (uint8_t)(Cmd & 0x3F);
  
  if(temp == OUTPUT_UPDOWN_Y20_ON) IOMsg.Y20_Down = IO_Distinguish;
  else if(temp == OUTPUT_KICK_Y21_ON) IOMsg.Y21_Kick = IO_Distinguish;
  else if(temp == OUTPUT_CHUCK_Y22_ON) IOMsg.Y22_Chuck = IO_Distinguish;
  else if(temp == OUTPUT_SWING_Y23_ON) IOMsg.Y23_Swing = IO_Distinguish;
  else if(temp == OUTPUT_SWING_RETURN_Y2F_ON) IOMsg.Y2F_SwingReturn = IO_Distinguish;
  else if(temp == OUTPUT_ROTATE_CHUCK_Y24_ON) IOMsg.Y24_ChuckRotation = IO_Distinguish;
  else if(temp == OUTPUT_VACCUM_Y25_ON) IOMsg.Y25_Vaccum = IO_Distinguish;
  else if(temp == OUTPUT_NIPPER_Y26_ON) IOMsg.Y26_Nipper = IO_Distinguish;
  else if(temp == OUTPUT_SUB_UPDOWN_Y2D_ON) IOMsg.Y2D_SubUp = IO_Distinguish;
  else if(temp == OUTPUT_SUB_KICK_Y2E_ON) IOMsg.Y2E_SubKick = IO_Distinguish;
  else if(temp == OUTPUT_SUB_CHUCK_Y27_ON) IOMsg.Y27_SubGrip = IO_Distinguish;
  else if(temp == OUTPUT_ALARM_Y28_ON) IOMsg.Y28_Alarm = IO_Distinguish;
  else if(temp == OUTPUT_MAIN_POWER_Y2G_ON) IOMsg.Y2G_MainPower = IO_Distinguish;
  
  
  else if(temp == ITLO_CYCLE_START_Y29_ON) IOMsg.Y29_CycleStart = IO_Distinguish;
  else if(temp == ITLO_MOLD_OPENCLOSE_Y2A_ON) IOMsg.Y2A_MoldOpenClose = IO_Distinguish;
  else if(temp == ITLO_EJECTOR_Y2B_ON) IOMsg.Y2B_Ejector = IO_Distinguish;
  else if(temp == ITLO_CONVEYOR_Y2C_ON)
  {
	 //	 IOMsg.Y2C_Conveyor = IO_Distinguish;
	 ControlMsg.fConveyor = CONVEYOR_START;
	 ControlMsg.TimerConveyor = ControlMsg.DelayMsg.DelayConveyor;
  }
  
  else if(temp == ITLO_BUZZER_Y28_ON) IOMsg.Y28_Buzzer = IO_Distinguish;	
  
  if(Refresh)
  {
	 EncodingOutput(&IOMsg);
	 WriteOutput(&Output);
  }
}
uint8_t GetInput(SEQ_ENUM InputSeq)
{
  uint8_t temp;
  uint8_t IO_Distinguish = 0; //1: ON, 0:OFF
  uint8_t ReadValue;
  
  ReadInput(&Input);
  DecodingInput(&IOMsg, &ControlMsg.TpEmr);
  
  temp = (uint8_t)(InputSeq & 0x3F);
  if(InputSeq < OUTPUT_UPDOWN_Y20_OFF) IO_Distinguish = 1;	//0x41
  
  if(temp == INPUT_UP_COMPLETE_X11_ON) ReadValue = IOMsg.X11_MainArmUpComplete;
  else if(temp == INPUT_CHUCK_OK_X16_ON) ReadValue = IOMsg.X16_MainChuckOk;
  else if(temp == INPUT_SWING_DONE_X14_ON) ReadValue = IOMsg.X14_SwingOk;
  else if(temp == INPUT_SWING_RETURN_DONE_X15_ON) ReadValue = IOMsg.X15_SwingReturnOk;
  else if(temp == INPUT_VACCUM_CHECK_X17_ON) ReadValue = IOMsg.X17_MainVaccumOk;
  else if(temp == INPUT_SUB_UP_COMPLETE_X1G_ON) ReadValue = IOMsg.X1G_SubUpComplete;
  else if(temp == INPUT_SUB_CHUCK_OK_X1F_ON) ReadValue = IOMsg.X1F_SubGripComplete;
  
  else if(temp == ITLI_FULLAUTO_X1H_ON) ReadValue = IOMsg.X1H_FullAuto;
  else if(temp == ITLI_AUTO_INJECTION_X19_ON) ReadValue = IOMsg.X19_AutoInjection;
  else if(temp == ITLI_MOLD_OPENED_X18_ON) ReadValue = IOMsg.X18_MoldOpenComplete;
  else if(temp == ITLI_SAFTY_DOOR_X1A_ON) ReadValue = IOMsg.X1A_SaftyDoor;
  else if(temp == ITLI_REJECT_X1B_ON) ReadValue = IOMsg.X1B_Reject;
  //  else if(temp == ITLI_EMO_IMM_X1I_ON) ReadValue = IOMsg.X1I_EMOFromIMM;
  if((ControlMsg.SCMode != MODE_AUTO) && ((InputSeq == INPUT_VACCUM_CHECK_X17_ON) || (InputSeq == INPUT_CHUCK_OK_X16_ON) ||
														(InputSeq == INPUT_SUB_CHUCK_OK_X1F_ON) )) return I_TRUE;
#ifdef CODE_ERROR_SENSOR_TIMEOUT
  if(ControlMsg.ErrorTime > 0)
  {
	 if(ControlMsg.InputTimeState == IN_TIME_STATE_START)
	 {
		if(ChkExpireSysTick(&ControlMsg.InTimeOutTick)) ErrorInputTimeOutProcess(InputSeq);
	 }
  }
#endif
  if(ReadValue == IO_Distinguish)
  {
#ifdef CODE_ERROR_SENSOR_TIMEOUT	  
	 ControlMsg.InputTimeState = IN_TIME_STATE_STANDBY;
#endif
	 return I_TRUE;
  }
  else  return I_FALSE;
}
uint8_t WaitDelay(SEQ_ENUM DelaySeq)
{
  static uint64_t TempSeqDelayTick = 0;
  static uint64_t StartTime = 0;					//StartTime
  //  static SEQ_ENUM pastDelaySeq = (SEQ_ENUM)0xEE;
  static uint8_t DelayTime = 0x00;	
  
  //	if(DelaySeq < DELAY_DOWN) return D_ERROR;
  //	printf("DelaySeq : %x\n\r", DelaySeq);
  //	printf("pastDelaySeq : %x\n\r", pastDelaySeq);
  
  if(pastDelaySeq != DelaySeq)
  {
	 pastDelaySeq = DelaySeq;
	 if(DelaySeq == DELAY_DOWN) DelayTime = ControlMsg.DelayMsg.DelayDown;
	 else if(DelaySeq == DELAY_KICK) DelayTime = ControlMsg.DelayMsg.DelayKick;
	 else if(DelaySeq == DELAY_EJECTOR) DelayTime = ControlMsg.DelayMsg.DelayEjector;
	 else if(DelaySeq == DELAY_CHUCK) DelayTime = ControlMsg.DelayMsg.DelayChuck;
	 else if(DelaySeq == DELAY_KICK_RETURN) DelayTime = ControlMsg.DelayMsg.DelayKickReturn;
	 else if(DelaySeq == DELAY_UP) DelayTime = ControlMsg.DelayMsg.DelayUp;
	 else if(DelaySeq == DELAY_SWING) DelayTime = ControlMsg.DelayMsg.DelaySwing;
	 else if(DelaySeq == DELAY_DOWN2ND) DelayTime = ControlMsg.DelayMsg.DelayDown2nd;
	 else if(DelaySeq == DELAY_OPEN) DelayTime = ControlMsg.DelayMsg.DelayOpen;
	 else if(DelaySeq == DELAY_UP2ND) DelayTime = ControlMsg.DelayMsg.DelayUp2nd;
	 else if(DelaySeq == DELAY_CHUCK_ROTATE_RETURN) DelayTime = ControlMsg.DelayMsg.DelayChuckRotateReturn;
	 else if(DelaySeq == DELAY_SWING_RETURN) DelayTime = ControlMsg.DelayMsg.DelaySwingReturn;
	 else if(DelaySeq == DELAY_NIPPER) DelayTime = ControlMsg.DelayMsg.DelayNipper;
	 //	 else if(DelaySeq == DELAY_CONVEYOR) DelayTime = ControlMsg.DelayMsg.DelayConveyor;
	 
	 SetSysTick(&TempSeqDelayTick, (uint64_t)((uint64_t)DelayTime*100));
	 SetStopWatch(&StartTime);
	 
	 return D_FALSE;
  }
  else
  {
	 //	  printf("RealTimerRemain : %x\n\r", ControlMsg.RealTimerRemain);
	 //	  printf("DelayTime : %x\n\r", DelayTime);
	 
	 ControlMsg.RealTimerRemain = (RemainStopWatch(&StartTime, (uint64_t)((uint64_t)DelayTime*100))/100);
	 if(!ChkExpireSysTick(&TempSeqDelayTick)) return D_FALSE;
	 else
	 {
		//			pastDelaySeq = DelaySeq;
		pastDelaySeq = (SEQ_ENUM)0xEE;
		DelayTime = 0x00;
		ControlMsg.RealTimerRemain = 0;
		return D_TRUE;
	 }
  }
}

uint8_t DistinguishSeq(SEQ_ENUM Seq)
{
  if(((Seq == ITLO_CYCLE_START_Y29_ON_FIRST)  && (AutoCycleCount == FIRST_START)) || ((Seq == ITLO_MOLD_OPENCLOSE_Y2A_ON_FIRST)  && (AutoCycleCount == FIRST_START))) 
	 return SEQ_ELSE;
  else if(Seq >= DELAY_DOWN) return SEQ_DELAY;
  else if((Seq & 0x3F) < INPUT_UP_COMPLETE_X11_ON) return SEQ_OUTPUT;
  else return SEQ_INPUT;
}

void LoopHoming(void)
{
  static uint64_t TempHomingTick = 0;
  static uint8_t ArmChecker = 0;
  static enum
  {
	 HOMING_INIT = 0,
	 
	 OUT_KICK_RETURN,
	 WAIT_KICK_RETURN,	//Not Use
	 
	 OUT_UP,
	 WAIT_UP,	 
	 
	 OUT_SWING_RETURN,
	 WAIT_SWING_RETURN,
	 
	 OUT_ROTATE_CHUCK_RETURN,
	 
	 HOMING_COMPLETE,
	 
  }HomingLoopState = HOMING_INIT;
  
  //	if(ControlMsg.fHoming == HOMING_DONE)
  //	{
  //		ControlMsg.SeqPos = 0x00;
  //		HAL_GPIO_TogglePin(LED3_GPIO_Port, LED3_Pin);
  //		HomingLoopState = HOMING_INIT;
  //		return LOOP_DONE;
  //	}
  
  switch(HomingLoopState)
  {
  case HOMING_INIT:
	 ControlMsg.InputTimeState = IN_TIME_STATE_STANDBY;
	 HAL_GPIO_WritePin(LED3_GPIO_Port, LED3_Pin, GPIO_PIN_SET);
	 //		HAL_GPIO_TogglePin(LED3_GPIO_Port, LED3_Pin);
	 ControlMsg.fHoming = HOMING_ING;
	 ControlMsg.SeqPos = 0x00;
	 
	 SetOutput(OUTPUT_CHUCK_Y22_OFF, REFRESH_ON);
	 SetOutput(OUTPUT_VACCUM_Y25_OFF, REFRESH_ON);
	 SetOutput(OUTPUT_SUB_CHUCK_Y27_OFF, REFRESH_ON);
	 SetOutput(OUTPUT_ALARM_Y28_OFF, REFRESH_ON);
	 
	 HomingLoopState = OUT_KICK_RETURN;
	 //	 	  printf("HomingLoopState : %d\n",HomingLoopState);
	 break;
	 
  case OUT_KICK_RETURN:
	 if(ControlMsg.MainArmDownPos) SetOutput(OUTPUT_KICK_Y21_OFF, REFRESH_ON);
	 else SetOutput(OUTPUT_KICK_Y21_ON, REFRESH_ON);
	 
	 if(ControlMsg.SubArmDownPos) SetOutput(OUTPUT_SUB_KICK_Y2E_ON, REFRESH_ON);
	 else SetOutput(OUTPUT_SUB_KICK_Y2E_OFF, REFRESH_ON);
	 
	 SetSysTick(&TempHomingTick, TIME_HOMING_KICK);
	 
	 HomingLoopState = OUT_UP;
	 //	     printf("HomingLoopState : %d\n",HomingLoopState);
	 break;
	 
  case OUT_UP:
	 if(!ChkExpireSysTick(&TempHomingTick)) break;
	 SetOutput(OUTPUT_UPDOWN_Y20_OFF, REFRESH_ON);
	 SetOutput(OUTPUT_SUB_UPDOWN_Y2D_OFF, REFRESH_ON);
	 SetSysTick(&TempHomingTick, TIME_HOMING_UP);
	 HomingLoopState = WAIT_UP;
	 ArmChecker = 0;
#ifdef CODE_ERROR_SENSOR_TIMEOUT
	 //	 if(ControlMsg.ErrorTime > 0)
	 //	 {
	 //		ControlMsg.InputTimeState = IN_TIME_STATE_STANDBY;
	 //		SetSysTick(&ControlMsg.InTimeOutTick, (uint64_t)((uint64_t)ControlMsg.ErrorTime*1000));
	 //	 }
	 if(ControlMsg.ErrorTime > 0)
	 {
		if(ControlMsg.InputTimeState == IN_TIME_STATE_STANDBY)
		{
		  ControlMsg.InputTimeState = IN_TIME_STATE_START;
		  SetSysTick(&ControlMsg.InTimeOutTick, (uint64_t)((uint64_t)ControlMsg.ErrorTime*1000));
		}
	 }
#endif
	 //	 	  printf("HomingLoopState : %d\n",HomingLoopState);
	 break;
	 
  case WAIT_UP:
	 //	  printf("HomingLoopState : %d\n",HomingLoopState);
	 if(fHomingSEQ == 1)
	 {
		HomingLoopState = HOMING_INIT;
		fHomingSEQ = 0;
	 }
	 else if(fHomingSEQ == 0)
	 {
		if(!ChkExpireSysTick(&TempHomingTick)) break;
		if(ArmChecker == 0)
		{
		  if(ControlMsg.MotionArm == ROBOT_MAIN_MAIN)
		  {
			 if(!GetInput(INPUT_UP_COMPLETE_X11_ON)) break;     //NEED CORRECT
		  }
		  else if(ControlMsg.MotionArm == ROBOT_MAIN_SUB)
		  {
			 if(!GetInput(INPUT_SUB_UP_COMPLETE_X1G_ON)) break;   //NEED CORRECT
		  }
		  else if(ControlMsg.MotionArm == ROBOT_MAIN_MS)
		  {
			 if(!GetInput(INPUT_UP_COMPLETE_X11_ON)) break;     //NEED CORRECT
			 if(ControlMsg.ErrorTime > 0)
			 {
				if(ControlMsg.InputTimeState == IN_TIME_STATE_STANDBY)
				{
				  ControlMsg.InputTimeState = IN_TIME_STATE_START;
				  ArmChecker = 1;
				  SetSysTick(&ControlMsg.InTimeOutTick, (uint64_t)((uint64_t)ControlMsg.ErrorTime*1000));
				}
			 }
		  }
		}
		//    else
		if(ArmChecker == 1)
		{
		  if(ControlMsg.MotionArm == ROBOT_MAIN_MS)
		  {
			 if(!GetInput(INPUT_SUB_UP_COMPLETE_X1G_ON)) break;   //NEED CORRECT
			 else ControlMsg.InputTimeState = IN_TIME_STATE_STANDBY;
		  }
		}
		HomingLoopState = OUT_SWING_RETURN;
		//     printf("HomingLoopState : %d\n",HomingLoopState);
	 }
	 break;
	 
  case OUT_SWING_RETURN:
	 if(ControlMsg.OutsideWait) 
	 {
		SetOutput(OUTPUT_SWING_Y23_ON, REFRESH_ON);
		SetOutput(OUTPUT_SWING_RETURN_Y2F_OFF, REFRESH_ON);
	 }
	 else
	 {
		SetOutput(OUTPUT_SWING_Y23_OFF, REFRESH_ON);
		SetOutput(OUTPUT_SWING_RETURN_Y2F_ON, REFRESH_ON);
	 }
	 
	 //	  SetSysTick(&TempHomingTick, TIMEOUT_INIT);
	 HomingLoopState = WAIT_SWING_RETURN;
	 //	  printf("HomingLoopState : %d\n",HomingLoopState);
	 
#ifdef CODE_ERROR_SENSOR_TIMEOUT
	 //	 if(ControlMsg.ErrorTime > 0)
	 //	 {
	 //		ControlMsg.InputTimeState = IN_TIME_STATE_STANDBY;
	 //		SetSysTick(&ControlMsg.InTimeOutTick, (uint64_t)((uint64_t)ControlMsg.ErrorTime*1000));
	 //	 }
	 if(ControlMsg.ErrorTime > 0)
	 {
		if(ControlMsg.InputTimeState == IN_TIME_STATE_STANDBY)
		{
		  ControlMsg.InputTimeState = IN_TIME_STATE_START;
		  SetSysTick(&ControlMsg.InTimeOutTick, (uint64_t)((uint64_t)ControlMsg.ErrorTime*1000));
		}
	 }
#endif
	 break;
	 
  case WAIT_SWING_RETURN:
	 if(fHomingSEQ == 1)
	 {
		HomingLoopState = HOMING_INIT;
		fHomingSEQ = 0;
	 }
	 else if(fHomingSEQ == 0)
	 {
		if(ControlMsg.OutsideWait == USE)
		{
		  if(!GetInput(INPUT_SWING_DONE_X14_ON)) break;
		}
		else if(ControlMsg.OutsideWait == NO_USE)
		{
		  if(!GetInput(INPUT_SWING_RETURN_DONE_X15_ON)) break;
		}
		
#ifndef CODE_SWING_ALWAYS
		SetOutput(OUTPUT_SWING_Y23_OFF, REFRESH_ON);
		SetOutput(OUTPUT_SWING_RETURN_Y2F_OFF, REFRESH_ON);
#endif
		HomingLoopState = OUT_ROTATE_CHUCK_RETURN;
		//	  printf("HomingLoopState : %d\n",HomingLoopState);
	 }
	 break;
	 
  case OUT_ROTATE_CHUCK_RETURN:
	 SetOutput(OUTPUT_ROTATE_CHUCK_Y24_OFF, REFRESH_ON);
	 
	 SetSysTick(&TempHomingTick, TIME_CHUCK_RR);
	 HomingLoopState = HOMING_COMPLETE;
	 //	  printf("HomingLoopState : %d\n",HomingLoopState);
	 break;
	 
  case HOMING_COMPLETE:
	 if(!ChkExpireSysTick(&TempHomingTick)) break;
	 
	 HAL_GPIO_WritePin(LED3_GPIO_Port, LED3_Pin, GPIO_PIN_RESET);
	 
	 ControlMsg.fHoming = HOMING_DONE;
	 //	  printf("Homing_Done\n\n\n");
	 HomingLoopState = HOMING_INIT;
         if(IOMsg.Y2A_MoldOpenClose == SIGNAL_ON) IOMsg.Y2A_MoldOpenClose = SIGNAL_OFF; // �������ͷ�
	 break;
	 
  }
  //	if(ControlMsg.fHoming == HOMING_ING) return LOOP_RUN;
  //	else return LOOP_DONE;
}



void LoopAppIO(void)
{
  static uint64_t TempTick = 0;
  //  static uint32_t pastOutput = 0x00;
  static enum
  {
	 INIT = 0,
	 
	 STANDBY_INIT,
	 HOMING,
	 
	 INPUT_UPDATE,
	 
	 OUTPUT_COMPARE,
	 OUTPUT_UPDATE,
  }IOLoopState = INIT; 
  
  switch(IOLoopState)
  {
  case INIT:
	 SetOutput(ITLO_MOLD_OPENCLOSE_Y2A_ON, REFRESH_ON);
	 SetOutput(ITLO_EJECTOR_Y2B_ON, REFRESH_ON);
	 MakeInitState(&IOMsg);
	 IOLoopState = STANDBY_INIT;
	 
  case STANDBY_INIT:
	 if(ControlMsg.SCMode > MODE_INIT)
	 {
#ifdef CODE_ERROR_SENSOR_TIMEOUT		 
		//		ControlMsg.InputTimeState = IN_TIME_STATE_STANDBY;
#endif
		ControlMsg.fHoming = HOMING_ING;
		IOLoopState = HOMING;
	 }
	 
	 break;
	 
  case HOMING:
	 if(ControlMsg.fHoming == HOMING_ING)
	 {
#ifdef CODE_BOOT_HOMING
		LoopHoming();
#else
		ControlMsg.fHoming = HOMING_DONE;
#endif
	 }
	 else
	 {
		SetSysTick(&TempTick, TIME_IO);
		IOLoopState = INPUT_UPDATE;
		//		printf("Homing IO Done -> go to IO Loop\n\r");
	 }
	 break;
	 
  case INPUT_UPDATE:
	 if(!ChkExpireSysTick(&TempTick)) break;
	 ReadInput(&Input);
	 DecodingInput(&IOMsg, &ControlMsg.TpEmr);
	 
#ifdef CODE_ERROR
	 if(IOMsg.X14_SwingOk == SIGNAL_ON && IOMsg.X15_SwingReturnOk == SIGNAL_ON)		//Swing Sensor
		ErrorProcess(ERRCODE_SWING_DUPLICATE);
#endif
	 SetSysTick(&TempTick, TIME_IO);
	 IOLoopState = OUTPUT_UPDATE;//OUTPUT_COMPARE;
	 break;
	 
	 //  case OUTPUT_COMPARE:
	 //	 if(pastOutput == (uint32_t)(Output | (Conveyor < 16)))
	 //	 {
	 //		IOLoopState = INPUT_UPDATE;
	 //	 }
	 //	 else IOLoopState = OUTPUT_UPDATE;
	 //	 break;
	 //	 
  case OUTPUT_UPDATE:
	 //	 pastOutput = (uint32_t)(Output | (Conveyor < 16));
	 EncodingOutput(&IOMsg);
	 WriteOutput(&Output);
	 
	 IOLoopState = INPUT_UPDATE;
	 
#ifdef CODE_ERROR_FAIL_TAKE_OUT	 												//In Operation Mode, Itl Error & DetectFail
	 //	 	 if(ControlMsg.SCMode < MODE_MANUAL) break;
	 //	 	 
	 //	 	 if(IOMsg.Y2A_MoldOpenClose == SIGNAL_ON && IOMsg.X18_MoldOpenComplete == SIGNAL_ON)
	 //	 	 {
	 //	 		 ErrorProcess(ERRCODE_MO_SENSOR);
	 //	 	 }
	 
	 //	 	 if(IOMsg.Y20_Down == SIGNAL_ON && IOMsg.X18_MoldOpenComplete == SIGNAL_OFF)
	 //	 	 {
	 //	 		 ErrorProcess(ERRCODE_MO_SENS_MISS);
	 //	 	 }
	 if(ControlMsg.MotionArm == ROBOT_MAIN_MAIN)
	 {
		if(IOMsg.Y20_Down == SIGNAL_ON && IOMsg.X18_MoldOpenComplete == SIGNAL_OFF && IOMsg.X15_SwingReturnOk == SIGNAL_ON)
		{
		  ErrorProcess(ERRCODE_MO_SENS_MISS);
		}
	 }
	 else if(ControlMsg.MotionArm == ROBOT_MAIN_SUB)
	 {
		if(IOMsg.Y2D_SubUp == SIGNAL_ON && IOMsg.X18_MoldOpenComplete == SIGNAL_OFF && IOMsg.X15_SwingReturnOk == SIGNAL_ON)
		{
		  ErrorProcess(ERRCODE_MO_SENS_MISS);
		}
	 }
	 else if(ControlMsg.MotionArm == ROBOT_MAIN_MS)
	 {
		if(IOMsg.Y20_Down == SIGNAL_ON && IOMsg.X18_MoldOpenComplete == SIGNAL_OFF && IOMsg.X15_SwingReturnOk == SIGNAL_ON)
		{
		  ErrorProcess(ERRCODE_MO_SENS_MISS);
		}
		else if(IOMsg.Y2D_SubUp == SIGNAL_ON && IOMsg.X18_MoldOpenComplete == SIGNAL_OFF && IOMsg.X15_SwingReturnOk == SIGNAL_ON)
		{
		  ErrorProcess(ERRCODE_MO_SENS_MISS);
		}
	 }
	 
	 
	 if(ControlMsg.fErrSaftyDoor == HOLD_ERROR_STATE_OCCUR && IOMsg.X1A_SaftyDoor == SIGNAL_OFF && ControlMsg.SaftyDoor == USE)
	 {
		ControlMsg.ErrState = ERROR_STATE_CLEAR;
		ControlMsg.fErrSaftyDoor++;		//HOLD_ERROR_STATE_SILENT_BUZZER
	 }
#endif
	 break;
  }
}
void StepPosProcess(void)
{
  if(ControlMsg.StepStandby == STEP_ING)
  {
	 if(ControlMsg.SeqPos < ControlMsg.StepSeq) ControlMsg.SeqPos++;
  }
  else //ControlMsg.StepStandby == STEP_END
  {
	 if(ControlMsg.SeqPos > ControlMsg.StepSeq) 
	 {
		ControlMsg.SeqPos = 0;
		ControlMsg.StepStandby = STEP_ING;
	 }
  }
}

void RunSeq(MODE_ENUM Mode)
{
  //  printf("SeqType : %x\n\r", DistinguishSeq(ControlMsg.SeqArr[ControlMsg.SeqPos]));
  //  printf("Mode : %x\n\r", Mode);
  //	printf("DistinguishSeq : %x\n\r", DistinguishSeq(ControlMsg.SeqArr[ControlMsg.SeqPos]));
#ifdef CODE_STEP_SEQ_ITL
  if(ControlMsg.SeqArr[ControlMsg.SeqPos] == ITLO_CYCLE_START_Y29_ON ||
	  ControlMsg.SeqArr[ControlMsg.SeqPos] == ITLO_CYCLE_START_Y29_ON_FIRST ||
		 ControlMsg.SeqArr[ControlMsg.SeqPos] == ITLO_MOLD_OPENCLOSE_Y2A_ON||
			ControlMsg.SeqArr[ControlMsg.SeqPos] == ITLO_MOLD_OPENCLOSE_Y2A_ON_FIRST||
			  ControlMsg.SeqArr[ControlMsg.SeqPos] == ITLO_EJECTOR_Y2B_ON||
				 //      ControlMsg.SeqArr[ControlMsg.SeqPos] == ITLO_CONVEYOR_Y2C_ON||
				 ControlMsg.SeqArr[ControlMsg.SeqPos] == ITLO_CYCLE_START_Y29_OFF||
					ControlMsg.SeqArr[ControlMsg.SeqPos] == ITLO_MOLD_OPENCLOSE_Y2A_OFF ||
					  ControlMsg.SeqArr[ControlMsg.SeqPos] == ITLO_EJECTOR_Y2B_OFF ||
						 ControlMsg.SeqArr[ControlMsg.SeqPos] == ITLO_CONVEYOR_Y2C_OFF ||
							ControlMsg.SeqArr[ControlMsg.SeqPos] == ITLI_FULLAUTO_X1H_OFF ||
							  ControlMsg.SeqArr[ControlMsg.SeqPos] == ITLI_AUTO_INJECTION_X19_OFF||
								 ControlMsg.SeqArr[ControlMsg.SeqPos] == ITLI_MOLD_OPENED_X18_OFF||
									ControlMsg.SeqArr[ControlMsg.SeqPos] == ITLI_SAFTY_DOOR_X1A_OFF||
									  ControlMsg.SeqArr[ControlMsg.SeqPos] == ITLI_REJECT_X1B_OFF||
										 ControlMsg.SeqArr[ControlMsg.SeqPos] == ITLI_FULLAUTO_X1H_ON||
											ControlMsg.SeqArr[ControlMsg.SeqPos] == ITLI_AUTO_INJECTION_X19_ON||
											  ControlMsg.SeqArr[ControlMsg.SeqPos] == ITLI_SAFTY_DOOR_X1A_ON||
												 ControlMsg.SeqArr[ControlMsg.SeqPos] == ITLI_REJECT_X1B_ON)
  { 
	 if(Mode == MODE_STEP)
	 {
//           if(IOMsg.Y2A_MoldOpenClose == SIGNAL_ON) IOMsg.Y2A_MoldOpenClose = SIGNAL_OFF; // �������ͷ�
		StepPosProcess();
		return;
	 }
	 else if(Mode == MODE_CYCLE)
	 {
//           if(IOMsg.Y2A_MoldOpenClose == SIGNAL_ON) IOMsg.Y2A_MoldOpenClose = SIGNAL_OFF; // �������ͷ�
		ControlMsg.SeqPos++;
		return;
	 }
  }
#endif
  
  if(DistinguishSeq(ControlMsg.SeqArr[ControlMsg.SeqPos]) == SEQ_INPUT)
  {
#ifdef CODE_ERROR_SENSOR_TIMEOUT	
	 if(ControlMsg.ErrorTime > 0)
	 {
		if(ControlMsg.InputTimeState == IN_TIME_STATE_STANDBY)
		{
		  ControlMsg.InputTimeState = IN_TIME_STATE_START;
		  SetSysTick(&ControlMsg.InTimeOutTick, (uint64_t)((uint64_t)ControlMsg.ErrorTime*1000));
		}
	 }
#endif
	 if(ControlMsg.SCMode != MODE_AUTO && ControlMsg.SeqArr[ControlMsg.SeqPos] == ITLI_MOLD_OPENED_X18_OFF)
	 {
		if(Mode == MODE_AUTO) ControlMsg.SeqPos++;
		else if(Mode == MODE_CYCLE) ControlMsg.SeqPos++;
		else if(Mode == MODE_STEP) StepPosProcess();
	 }
	 else if((ControlMsg.SCMode == MODE_AUTO) && (AutoCycleCount == SECOND_CYCLE) && ((MoldOpenCloseCheck == MOLD_OPEN) || (AutoInjectionTemp == MOLD_OPEN)))
	 {
		if ((ControlMsg.SeqArr[ControlMsg.SeqPos] == ITLI_MOLD_OPENED_X18_OFF) || (ControlMsg.SeqArr[ControlMsg.SeqPos] == ITLI_AUTO_INJECTION_X19_ON))
		  ControlMsg.SeqPos++;
		else
		{
		  if(GetInput(ControlMsg.SeqArr[ControlMsg.SeqPos])) ControlMsg.SeqPos++;
		}
	 }
	 else if(GetInput(ControlMsg.SeqArr[ControlMsg.SeqPos]))
	 {
		if(Mode == MODE_AUTO) ControlMsg.SeqPos++;
		else if(Mode == MODE_CYCLE) ControlMsg.SeqPos++;
		else if(Mode == MODE_STEP) StepPosProcess();
	 }
  }
  else if(DistinguishSeq(ControlMsg.SeqArr[ControlMsg.SeqPos]) == SEQ_OUTPUT)
  {
	 ControlMsg.InputTimeState = IN_TIME_STATE_STANDBY;
	 SetOutput(ControlMsg.SeqArr[ControlMsg.SeqPos], REFRESH_0FF);
	 if(Mode == MODE_AUTO) ControlMsg.SeqPos++;
	 else if(Mode == MODE_CYCLE) ControlMsg.SeqPos++;
	 else if(Mode == MODE_STEP) StepPosProcess();
  }
  else if(DistinguishSeq(ControlMsg.SeqArr[ControlMsg.SeqPos]) == SEQ_ELSE) 
  {
	 IOMsg.Y29_CycleStart = SIGNAL_ON;
	 IOMsg.Y2A_MoldOpenClose = SIGNAL_ON;
	 ControlMsg.SeqPos++;
  }
  else// if(DistinguishSeq(ControlMsg.SeqArr[ControlMsg.SeqPos]) == SEQ_DELAY)
  {
	 ControlMsg.InputTimeState = IN_TIME_STATE_STANDBY;
	 if(Mode == MODE_STEP) StepPosProcess();
	 else if(Mode == MODE_AUTO || Mode == MODE_CYCLE)
	 {
		if(WaitDelay(ControlMsg.SeqArr[ControlMsg.SeqPos]) == D_TRUE)
		  ControlMsg.SeqPos++;
	 }
  }
}

uint8_t ChkModeChange(MODE_ENUM newMode)//Check Mode Change
{
  static MODE_ENUM oldMode;
  if(oldMode == newMode) return SEQ_MODE_STAY;
  
  if(oldMode == MODE_MANUAL)
  {
	 if(newMode == MODE_STEP || newMode == MODE_CYCLE || newMode == MODE_AUTO)
	 {
		ControlMsg.fHoming = HOMING_ING;
		ControlMsg.SeqPos = 0x00;
		return SEQ_MODE_CHANGED; 			//newMode;
	 }
  }
  
  oldMode = newMode;
  
  return SEQ_MODE_STAY;
}
void LoopRunSeq(void)
{
  static uint64_t TempSeqTick = 0;
  static enum
  {
	 INIT = 0,
	 
	 MODE_CHECK,
	 
	 SEQ_MODE_HOMING,
	 SEQ_MODE_STEP,
	 SEQ_MODE_CYCLE,
	 SEQ_MODE_AUTO,
  }SeqLoopState = INIT;
  
  switch(SeqLoopState)
  {
  case INIT:
#ifdef CODE_ERROR_SENSOR_TIMEOUT
	 pastDelaySeq = (SEQ_ENUM)0xEE;
	 AutoCycleCount = FIRST_START;
	 MoldOpenCloseCheck = MOLD_INIT;
	 AutoInjectionTemp = MOLD_INIT;
	 ControlMsg.RealTimerRemain = 0;
	 //	 ControlMsg.SeqPos = 0;
	 //	 ControlMsg.InputTimeState = IN_TIME_STATE_STANDBY;
#endif
	 SetSysTick(&TempSeqTick, TIME_SEQ);
	 SeqLoopState = MODE_CHECK;
	 break;
	 
  case MODE_CHECK:
	 if(!ChkExpireSysTick(&TempSeqTick)) break;
	 if(!(ControlMsg.ErrState == ERROR_STATE_NONE)) break;
	 
	 if(ControlMsg.SCMode == MODE_STEP) SeqLoopState = SEQ_MODE_STEP;
	 else if(ControlMsg.SCMode == MODE_CYCLE || ControlMsg.SCMode == MODE_CYCLE_PAUSE) SeqLoopState = SEQ_MODE_CYCLE;
	 else if(ControlMsg.SCMode == MODE_AUTO) SeqLoopState = SEQ_MODE_AUTO;
	 
	 if(ControlMsg.DoHoming == 1)
	 {
		ControlMsg.fHoming = HOMING_ING;
		SeqLoopState = SEQ_MODE_HOMING;
	 }
	 SetSysTick(&TempSeqTick, TIME_SEQ);
	 break;
	 
  case SEQ_MODE_HOMING:
	 if(!ChkExpireSysTick(&TempSeqTick)) break;
	 if(ControlMsg.fHoming == HOMING_ING)
	 {
		LoopHoming();
	 }
	 else SeqLoopState = INIT;
	 break;
	 
  case SEQ_MODE_STEP:
	 if(ControlMsg.SCMode != MODE_STEP)
	 {
		SeqLoopState = INIT;
		break;
	 }
	 if(!ChkExpireSysTick(&TempSeqTick)) break;
	 
	 if(ControlMsg.fHoming == HOMING_ING)
	 {
		LoopHoming();
	 }
	 else
	 {
		SetSysTick(&TempSeqTick, TIME_SEQ);
		RunSeq(ControlMsg.SCMode);
		if(ControlMsg.SeqArr[ControlMsg.SeqPos] == SEQ_END)
		{
		  ControlMsg.StepStandby = STEP_END;
		}
	 }
	 break;
	 
  case SEQ_MODE_CYCLE:
	 if(!(ControlMsg.SCMode == MODE_CYCLE || ControlMsg.SCMode == MODE_CYCLE_PAUSE))
	 {
		SeqLoopState = INIT;
		break;
	 }
	 if(!ChkExpireSysTick(&TempSeqTick)) break;
	 if(ControlMsg.fHoming == HOMING_ING)
	 {
		LoopHoming();
	 }
	 else
	 {
		SetSysTick(&TempSeqTick, TIME_SEQ);
		RunSeq(ControlMsg.SCMode);
		if(ControlMsg.SeqArr[ControlMsg.SeqPos] == SEQ_END)
		{
		  ControlMsg.SeqPos = 0;
		  ControlMsg.SCMode = MODE_MANUAL;
		}
	 }
	 break;
	 
	 
  case SEQ_MODE_AUTO:
	 if(ControlMsg.SCMode != MODE_AUTO)
	 {
		SeqLoopState = INIT;
		break;
	 }
	 if(!ChkExpireSysTick(&TempSeqTick)) break;
	 
	 if(ControlMsg.fHoming == HOMING_ING)
	 {
		LoopHoming();
	 }
	 else
	 {
		SetSysTick(&TempSeqTick, TIME_SEQ);
		RunSeq(ControlMsg.SCMode);
		
#ifdef CODE_ERROR_PROCESS_TIME
		if(ControlMsg.ProcessTime > 0)
		{
		  if(ControlMsg.SeqPos < 2)
			 SetSysTick(&ControlMsg.ProcessTimeTick, (uint64_t)((uint64_t)ControlMsg.ProcessTime*1000));
		}
#endif
		if(ControlMsg.SeqArr[ControlMsg.SeqPos] == SEQ_END)
		{
		  ControlMsg.SeqPos = 0;
		  ControlMsg.Count.TotalCnt++;
		  AutoCycleCount = SECOND_CYCLE;
#ifdef CODE_ERROR_PROCESS_TIME
		  if(ControlMsg.ProcessTime > 0)
		  {
			 if(ChkExpireSysTick(&ControlMsg.ProcessTimeTick))
			 {
				ErrorProcess(ERRCODE_PROCESS_TIME);
			 }
		  }
#endif
		}
	 }
	 //Standard
	 if(ControlMsg.Setting.ItlAutoInjection == NO_USE)
	 {
		if(IOMsg.X18_MoldOpenComplete == SIGNAL_OFF)
		{
		  IOMsg.Y29_CycleStart = SIGNAL_OFF;
		  MoldOpenCloseCheck = MOLD_CLOSE;
		}
		else if((MoldOpenCloseCheck == MOLD_CLOSE) && (IOMsg.X18_MoldOpenComplete == SIGNAL_ON)) MoldOpenCloseCheck = MOLD_OPEN;
		if(IOMsg.Y29_CycleStart == SIGNAL_ON) MoldOpenCloseCheck = MOLD_INIT;
	 }
	 //EuropMap
	 else if(ControlMsg.Setting.ItlAutoInjection == USE)
	 {
		if(IOMsg.X19_AutoInjection == SIGNAL_ON)
		  AutoInjectionTemp = MOLD_CLOSE;
		else if((AutoInjectionTemp == MOLD_CLOSE) && (IOMsg.X19_AutoInjection == SIGNAL_OFF) && (IOMsg.X18_MoldOpenComplete == SIGNAL_ON)) 
		{
		  IOMsg.Y2A_MoldOpenClose = SIGNAL_OFF;
		  AutoInjectionTemp = MOLD_OPEN;
		}
		else if((AutoInjectionTemp == MOLD_OPEN) && (IOMsg.Y2A_MoldOpenClose == SIGNAL_ON)) AutoInjectionTemp = MOLD_INIT;
	 }
	 break;
  }
  
  if(ControlMsg.fConveyor == CONVEYOR_STOP) return;
  {
	 static uint64_t ConveyorTick = 0;
	 static enum
	 {
		TIMER_SET = 0,
		STOP_CONVEYOR,
	 }ConveyorLoopState = TIMER_SET;
	 
	 switch(ConveyorLoopState)
	 {
	 case TIMER_SET:
		IOMsg.Y2C_Conveyor = SIGNAL_ON;
		SetSysTick(&ConveyorTick, (uint64_t)(ControlMsg.TimerConveyor)*100);
		ControlMsg.fConveyor = CONVEYOR_RUN;
		ConveyorLoopState = STOP_CONVEYOR;
		break;
		
	 case STOP_CONVEYOR:
		if(!ChkExpireSysTick(&ConveyorTick)) break;
		IOMsg.Y2C_Conveyor = SIGNAL_OFF;
		ControlMsg.fConveyor = CONVEYOR_STOP;
		ConveyorLoopState = TIMER_SET;
		break;
	 }
  }
}