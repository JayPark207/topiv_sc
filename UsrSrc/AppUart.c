/* Includes -------------------------------------------------------------------*/
#include "AppUart.h"
#include "string.h"
#include "stdlib.h"
/* typedef --------------------------------------------------------------------*/

/* define ---------------------------------------------------------------------*/
BUFF_TYPE BufU1Rx[QUEUE_SIZE];
BUFF_TYPE BufU1Tx[QUEUE_SIZE];

QUEUE	qU1Rx;
QUEUE	qU1Tx;
  
QUEUE *pqU1Rx;
QUEUE *pqU1Tx;

extern uint8_t u1RxData;

extern CONTROL_MSG ControlMsg;
extern IO_MSG IOMsg;

extern uint8_t fHomingSEQ;

MODBUS_MSG ModbusMsgRx;
MODBUS_MSG* pModbusMsgRx;
MODBUS_MSG ModbusMsgTx;
MODBUS_MSG* pModbusMsgTx;

/* macro ----------------------------------------------------------------------*/

/* function prototypes --------------------------------------------------------*/
void InitQueue(void);
/* ----------------------------------------------------------------------------*/


extern UART_HandleTypeDef huart1;
void ChkHoming(MODE_ENUM newMode)//Check Mode Change
{
	static MODE_ENUM oldMode;
	if(oldMode == newMode) return;
	
	if(oldMode == MODE_MANUAL)
	{
		if(newMode == MODE_STEP || newMode == MODE_CYCLE || newMode == MODE_AUTO)
		{
			ControlMsg.fHoming = HOMING_ING;
			ControlMsg.SeqPos = 0x00;
//			printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\r");
		}
	}
	oldMode = newMode;
}

void AddCRC(QUEUE* pQueue, uint8_t Count)
{
	uint8_t crc_hi = 0xFF; /* high CRC byte initialized */
   uint8_t crc_lo = 0xFF; /* low CRC byte initialized */
   unsigned int i, k, val; /* will index into CRC lookup */

    /* pass through message buffer */

//	printf("Count = %3d\n",Count);
	 for(i=0; i < Count; i++)
    {
        val = (*pQueue->pBuff)[i];
//		  printf("%5X\t", val);
        k = crc_hi ^ val;
        crc_hi = crc_lo ^ table_crc_hi[k];
        crc_lo = table_crc_lo[k];
    }
		 
	 WriteSingleByte(pQueue, crc_hi);
	 IncHead(pQueue, 1);	 
	 WriteSingleByte(pQueue, crc_lo);
	 IncHead(pQueue, 1);	 
	 
}
uint8_t CheckRCV(QUEUE* pQueue)
{
	
	uint8_t crc_hi = 0xFF; /* high CRC byte initialized */
   uint8_t crc_lo = 0xFF; /* low CRC byte initialized */
	uint16_t i, k, val; /* will index into CRC lookup */

	uint8_t cntQ;
	
	cntQ = GetExistCnt(pQueue);
//	if(cntQ < 8) return CRC_ERROR_MISS;
	
	/**************Check Slave Address*****************************/
	if(GetQueueData(pQueue, MODBUS_POS_SA) != MODBUS_SLAVE_ID) return SA_NOT_MATCH;

	/**************************************************************/
	for(i=0; i < cntQ-3; i++)
    {
        val = (*pQueue->pBuff)[i];
//		  printf("%5X\t", val);
        k = crc_hi ^ val;
        crc_hi = crc_lo ^ table_crc_hi[k];
        crc_lo = table_crc_lo[k];
    }
	if(crc_hi != (*pQueue->pBuff)[cntQ - 2]) return CRC_ERROR_HI;
	if(crc_lo != (*pQueue->pBuff)[cntQ - 1]) return CRC_ERROR_LOW;

	
	return CRC_ERROR_NONE;
}

void ExcIOMakeResp(QUEUE* pQRx,QUEUE* pQTx,MODBUS_MSG* pModbusMsgRx, MODBUS_MSG* pModbusMsgTx)
{
   MODE_ENUM Mode;
   static uint8_t fErrInit = 0;
   
   Mode = (MODE_ENUM)GetQueueData(pQRx, 2);
   ChkHoming(Mode);
   
   memcpy(pModbusMsgRx, pQRx->pBuff, 2);
   memcpy(pQTx->pBuff, pModbusMsgRx, 2);
   IncHead(pQTx, 2);
   
	
//      PrintQueue(pQRx);
   if(Mode == MODE_STANDBY)                                    //INIT//INIT//INIT//INIT//INIT//INIT//INIT//INIT//INIT
   {
      ControlMsg.SCMode = MODE_STANDBY;
      ControlMsg.OutsideWait = GetQueueData(pQRx, 3);
      ControlMsg.MainArmDownPos = GetQueueData(pQRx, 4);
      ControlMsg.SubArmDownPos = GetQueueData(pQRx, 5);
		ControlMsg.ErrorTime = GetQueueData(pQRx, 6);
		ControlMsg.ProcessTime = GetQueueData(pQRx, 7);
		ControlMsg.SaftyDoor = GetQueueData(pQRx, 8);
		ControlMsg.MotionArm = GetQueueData(pQRx, 9);
		ControlMsg.DoHoming = GetQueueData(pQRx, 10);
		ControlMsg.Setting.ItlAutoInjection = GetQueueData(pQRx, 11);		//Euromap Interlock
		ControlMsg.Buzzer = GetQueueData(pQRx, 12);		//Buzzer

#ifdef CODE_ERROR
		if(ControlMsg.ErrState == ERROR_STATE_OCCUR) WriteSingleByte(pQTx, MODE_ERROR);
		else WriteSingleByte(pQTx, Mode);
#else
		WriteSingleByte(pQTx, Mode);
#endif
      IncHead(pQTx, 1);
      memcpy(&(*pQTx->pBuff)[GetHead(pQTx)], (uint8_t*)(&IOMsg.Y20_Down), SIZE_IO);
      IncHead(pQTx, SIZE_IO);
   }
   else if(Mode == MODE_SEQ)                        //SEQ//SEQ//SEQ//SEQ//SEQ//SEQ//SEQ//SEQ//SEQ//SEQ//SEQ//SEQ
   {
	  if(ControlMsg.DoHoming != 0) ControlMsg.DoHoming = 0;
      ControlMsg.SeqPos = GetQueueData(pQRx, 3);
      memcpy((ControlMsg.SeqArr), ((uint8_t*)(pQRx->pBuff) + 4), SIZE_SEQARR);
      memcpy(&(ControlMsg.Count), ((uint8_t*)(pQRx->pBuff) + 4 + SIZE_SEQARR), SIZE_COUNT);
      memcpy(&(ControlMsg.DelayMsg), ((uint8_t*)(pQRx->pBuff) + 4 + SIZE_SEQARR + SIZE_COUNT),sizeof(DELAY_MSG));
      
#ifdef CODE_ERROR
		if(ControlMsg.ErrState == ERROR_STATE_OCCUR) WriteSingleByte(pQTx, MODE_ERROR);
		else WriteSingleByte(pQTx, Mode);
#else
		WriteSingleByte(pQTx, Mode);
#endif
		IncHead(pQTx, 1);
      WriteSingleByte(pQTx, ControlMsg.fHoming);
      IncHead(pQTx, 1);
		memcpy(&(*pQTx->pBuff)[GetHead(pQTx)], (uint8_t*)(&IOMsg.Y20_Down), SIZE_IO);
      IncHead(pQTx, SIZE_IO);
   }
   
   else if(Mode == MODE_MANUAL)                     //MANUAL//MANUAL//MANUAL//MANUAL//MANUAL//MANUAL//MANUAL//MANUAL
   {
	  if(ControlMsg.fHoming > 0) ControlMsg.fHoming = 0;
	  ControlMsg.SCMode = MODE_MANUAL;
	  //      memcpy(&IOMsg.Y20_Down, ((uint8_t*)(pQRx->pBuff) + 3), SIZE_OUTPUT);
//	  volatile uint8_t OrgBuzzer;
//	  OrgBuzzer = IOMsg.Y28_Alarm;
	  memcpy(&IOMsg.Y20_Down, ((uint8_t*)(pQRx->pBuff) + 3), (SIZE_OUTPUT-2));
//	  IOMsg.Y28_Alarm = OrgBuzzer;

#ifdef CODE_ERROR
		if(ControlMsg.ErrState == ERROR_STATE_OCCUR) WriteSingleByte(pQTx, MODE_ERROR);
		else WriteSingleByte(pQTx, Mode);
#else
		WriteSingleByte(pQTx, Mode);
#endif
      IncHead(pQTx, 1);
      memcpy(&(*pQTx->pBuff)[GetHead(pQTx)], (uint8_t*)(&IOMsg.Y20_Down), SIZE_IO);
      IncHead(pQTx, SIZE_IO);
		WriteSingleByte(pQTx, IOMsg.Y28_Buzzer);
      IncHead(pQTx, 1);
   }
   else if(Mode == MODE_STEP)                  //STEP//STEP//STEP//STEP//STEP//STEP//STEP//STEP//STEP
   {
      ControlMsg.SCMode = MODE_STEP;
      ControlMsg.StepSeq = GetQueueData(pQRx, 3);
      
      
#ifdef CODE_ERROR
		if(ControlMsg.ErrState == ERROR_STATE_OCCUR) WriteSingleByte(pQTx, MODE_ERROR);
		else WriteSingleByte(pQTx, Mode);
#else
		WriteSingleByte(pQTx, Mode);
#endif
      IncHead(pQTx, 1);
      memcpy(&(*pQTx->pBuff)[GetHead(pQTx)], (uint8_t*)(&IOMsg.Y20_Down), SIZE_IO);
      IncHead(pQTx, SIZE_IO);
      WriteSingleByte(pQTx, ControlMsg.SeqPos);
      IncHead(pQTx, 1);
		WriteSingleByte(pQTx, ControlMsg.fHoming);
      IncHead(pQTx, 1);
//		printf("step fHoming = %x\n\r",ControlMsg.fHoming);
   }
   else if(Mode == MODE_CYCLE_PAUSE)               //CYCLE//CYCLE//CYCLE//CYCLE//CYCLE//CYCLE//CYCLE//CYCLE//CYCLE
   {
      ControlMsg.SCMode = MODE_CYCLE_PAUSE;
		
#ifdef CODE_ERROR
		if(ControlMsg.ErrState == ERROR_STATE_OCCUR) WriteSingleByte(pQTx, MODE_ERROR);
		else WriteSingleByte(pQTx, Mode);
#else
		WriteSingleByte(pQTx, Mode);
#endif
      IncHead(pQTx, 1);
      memcpy(&(*pQTx->pBuff)[GetHead(pQTx)], (uint8_t*)(&IOMsg.Y20_Down), SIZE_IO);
      IncHead(pQTx, SIZE_IO);
      WriteSingleByte(pQTx, ControlMsg.SeqPos);
      IncHead(pQTx, 1);
      WriteSingleByte(pQTx, ControlMsg.RealTimerRemain);
      IncHead(pQTx, 1);
		WriteSingleByte(pQTx, ControlMsg.fHoming);
      IncHead(pQTx, 1);
   }
   else if(Mode == MODE_CYCLE)               //CYCLE//CYCLE//CYCLE//CYCLE//CYCLE//CYCLE//CYCLE//CYCLE//CYCLE
   {
      ControlMsg.SCMode = MODE_CYCLE;
		
#ifdef CODE_ERROR
		if(ControlMsg.ErrState == ERROR_STATE_OCCUR) WriteSingleByte(pQTx, MODE_ERROR);
		else WriteSingleByte(pQTx, Mode);
#else
		WriteSingleByte(pQTx, Mode);
#endif
      IncHead(pQTx, 1);
      memcpy(&(*pQTx->pBuff)[GetHead(pQTx)], (uint8_t*)(&IOMsg.Y20_Down), SIZE_IO);
      IncHead(pQTx, SIZE_IO);
      WriteSingleByte(pQTx, ControlMsg.SeqPos);
      IncHead(pQTx, 1);
      WriteSingleByte(pQTx, ControlMsg.RealTimerRemain);
      IncHead(pQTx, 1);
		WriteSingleByte(pQTx, ControlMsg.fHoming);
      IncHead(pQTx, 1);
		printf("cycle fHoming = %x\n\r",ControlMsg.fHoming);
   }
   else if(Mode == MODE_AUTO)               //AUTO//AUTO//AUTO//AUTO//AUTO//AUTO//AUTO//AUTO//AUTO//AUTO//AUTO
   {
      ControlMsg.SCMode = MODE_AUTO;
      memcpy(&(ControlMsg.DelayMsg), ((uint8_t*)(pQRx->pBuff) + 3), sizeof(DELAY_MSG));
      
#ifdef CODE_ERROR
		if(ControlMsg.ErrState == ERROR_STATE_OCCUR) WriteSingleByte(pQTx, MODE_ERROR);
		else WriteSingleByte(pQTx, Mode);
#else
		WriteSingleByte(pQTx, Mode);
#endif
      IncHead(pQTx, 1);
		
      memcpy(&(*pQTx->pBuff)[GetHead(pQTx)], (uint8_t*)(&IOMsg.Y20_Down), SIZE_IO);
      IncHead(pQTx, SIZE_IO);
      memcpy(&(*pQTx->pBuff)[GetHead(pQTx)], (uint8_t*)(&ControlMsg.Count), SIZE_COUNT);
      IncHead(pQTx, SIZE_COUNT);
      WriteSingleByte(pQTx, ControlMsg.SeqPos);
      IncHead(pQTx, 1);
      WriteSingleByte(pQTx, ControlMsg.RealTimerRemain);
      IncHead(pQTx, 1);
		WriteSingleByte(pQTx, ControlMsg.fHoming);
      IncHead(pQTx, 1);
		
//		printf("ControlMsg.SeqArr[ControlMsg.SeqPos] : %x\n",ControlMsg.SeqArr[ControlMsg.SeqPos]);
   }
	
	//	else if(Mode == MODE_ERROR)               //ERROR//ERROR//ERROR//ERROR//ERROR//ERROR//ERROR//ERROR
//	{
//		uint8_t tempErrState = 0xFF;
//		
////		ControlMsg.SCMode = MODE_ERROR;
//		//      ControlMsg.SCMode = MODE_STANDBY;
//		
////		ControlMsg.ErrState = GetQueueData(pQRx, 3);
////		if(GetQueueData(pQRx, 4) != 0) ControlMsg.ErrCode = GetQueueData(pQRx, 4);
#ifdef CODE_ERROR
	else if(Mode == MODE_ERROR)               //ERROR//ERROR//ERROR//ERROR//ERROR//ERROR//ERROR//ERROR
	{
		uint8_t tempErrState = 0;
		tempErrState = GetQueueData(pQRx, 3);
		if((ControlMsg.SCMode != MODE_ERROR) && (tempErrState == ERROR_STATE_OCCUR)) ControlMsg.SCMode = MODE_ERROR;
//		else if((ControlMsg.SCMode != MODE_STANDBY) && (tempErrState > ERROR_STATE_OCCUR)) ControlMsg.SCMode = MODE_STANDBY;
//		ControlMsg.ErrState = GetQueueData(pQRx, 3);
//		if(GetQueueData(pQRx, 4) != 0) ControlMsg.ErrCode = GetQueueData(pQRx, 4);
		
		WriteSingleByte(pQTx, Mode);		//2
		IncHead(pQTx, 1);
		
		if(tempErrState == ERROR_STATE_OCCUR)
		{
			if(ControlMsg.SaftyDoor == USE && ControlMsg.ErrState == ERROR_STATE_CLEAR)
			{
//			  ControlMsg.SCMode = MODE_STANDBY;
				ControlMsg.ErrState = ERROR_STATE_CLEAR;
			}
			else ControlMsg.ErrState = ERROR_STATE_OCCUR;
		}
		else if(tempErrState == ERROR_STATE_CLEAR)
		{
//		  ControlMsg.SCMode = MODE_STANDBY;
			ControlMsg.ErrState = ERROR_STATE_CLEAR;
		}
		else if(tempErrState == ERROR_STATE_ACCEPT)
		{
			ControlMsg.SCMode = MODE_STANDBY;
			fErrInit = 1;
			//		  ControlMsg.InputTimeState = IN_TIME_STATE_STANDBY;
			//			ControlMsg.ErrState = ERROR_STATE_NONE;
			ControlMsg.SeqPos = 0;
			//		  ControlMsg.InTimeOutTick = 0xFFFFFFFFFFFFFFFF;
			ControlMsg.ErrState = ERROR_STATE_NONE;
			
			
#ifdef CODE_ERROR_SENSOR_TIMEOUT
			
			if(ControlMsg.ErrorTime > 0)
			{
//				ControlMsg.InputTimeState = IN_TIME_STATE_STANDBY;
			  ControlMsg.InputTimeState = IN_TIME_STATE_START;
			  SetSysTick(&ControlMsg.InTimeOutTick, (uint64_t)((uint64_t)ControlMsg.ErrorTime*1000));
			}
#endif
			ControlMsg.ErrState = ERROR_STATE_ACCEPT;
		}
	
		WriteSingleByte(pQTx, ControlMsg.ErrState);		//3
		IncHead(pQTx, 1);
		if(fErrInit)
		{
		  ControlMsg.ErrState = ERROR_STATE_NONE;
		  fErrInit = 0;
		  fHomingSEQ = 1;
		  ControlMsg.InputTimeState = IN_TIME_STATE_STANDBY;
		}
		
		WriteSingleByte(pQTx, ControlMsg.ErrCode);		//4
		IncHead(pQTx, 1);
		
		memcpy(&(*pQTx->pBuff)[GetHead(pQTx)], (uint8_t*)(&ControlMsg.Count), SIZE_COUNT);	//Error Count
		IncHead(pQTx, SIZE_COUNT);
	}
#endif

//	printf("ErrorState : %x \t ErrCode : %x\n\r", ControlMsg.ErrState, ControlMsg.ErrCode);
//	printf("ErrorState : %x \t ErrCode : %x\n\r", ControlMsg.ErrState, ControlMsg.ErrCode);
   AddCRC(pQTx, pQTx->Head);
}

void SendPacket(QUEUE* pQueue)
{
	uint8_t	u1TxData;
	uint8_t CntOfQueue;
	
	CntOfQueue = GetExistCnt(pQueue);
	for(int i=0;i<CntOfQueue;i++)
	{
		u1TxData = GetQueueData(pQueue, i);
		HAL_UART_Transmit(&huart1, &u1TxData, 1, 5000);
	}
}

void LoopAppUart(void)
{
	static uint64_t TempTick;
	
	static enum
	{
		INIT = 0,
		
		STANDBY_RCV,			//MUST ADD WATCHDOG
		
		RCV_PACKET,				//AND Insert DATA
		
		MAKE_RESPONSE_PACKET,	//And Send the packet
		
		SEND_PACKET_DELAY,
		
	}UartLoopState = INIT;
	switch(UartLoopState)
	{
		case INIT:
		pModbusMsgRx = &ModbusMsgRx;
		pModbusMsgTx = &ModbusMsgTx;
		HAL_UART_Receive_IT(&huart1, &u1RxData, 1);
		InitQueue();
		ResetQueue(pqU1Tx);
		ResetQueue(pqU1Rx);
		
		UartLoopState = STANDBY_RCV;
		break;
		
		case STANDBY_RCV:
		if(IsEmpty(pqU1Rx))	break;
		
		SetSysTick(&TempTick, TIME_DELAY_RCV);
		UartLoopState = RCV_PACKET;
		break;
		
		case RCV_PACKET:
		if(!ChkExpireSysTick(&TempTick)) break;
//		PrintQueue(pqU1Rx);
		if(!CheckRCV(pqU1Rx))
		{
			ResetQueue(pqU1Rx);
			UartLoopState = STANDBY_RCV;
			break;
		}
		
		UartLoopState = MAKE_RESPONSE_PACKET;
		break;


		case MAKE_RESPONSE_PACKET:
		HAL_GPIO_TogglePin(LED1_GPIO_Port, LED1_Pin);
		
		ResetQueue(pqU1Tx);
		ExcIOMakeResp(pqU1Rx, pqU1Tx, pModbusMsgRx, pModbusMsgTx);
		SendPacket(pqU1Tx);
		

		SetSysTick(&TempTick, TIME_DELAY_SEND);
		UartLoopState = SEND_PACKET_DELAY;
		break;
		
		case SEND_PACKET_DELAY:
		if(!ChkExpireSysTick(&TempTick)) break;
		ResetQueue(pqU1Tx);
		ResetQueue(pqU1Rx);
		UartLoopState = STANDBY_RCV;
		break;

	}
	
}
void InitQueue(void)
{
	pqU1Tx = &qU1Tx;
	NewQueue(pqU1Tx, &BufU1Tx, QUEUE_SIZE);	
	pqU1Rx = &qU1Rx;
	NewQueue(pqU1Rx, &BufU1Rx, QUEUE_SIZE);	
}
