/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __APPSEQ_H
#define __APPSEQ_H
#ifdef __cplusplus
 extern "C" {
#endif
/* Includes -------------------------------------------------------------------*/

#include "interface.h"
#include "main.h"
#include "timer.h"
#include "Queue.h"

/* typedef --------------------------------------------------------------------*/


/* define ---------------------------------------------------------------------*/
   
#define SEQ_OUTPUT_ON_START 0x01
#define SEQ_OUTPUT_ON_END 0x0D
#define SEQ_OUTPUT_INTERLOCK_ON_START 0x0E
#define SEQ_OUTPUT_INTERLOCK_ON_END 0x12

#define SEQ_INPUT_ON_START 0x13
#define SEQ_INPUT_ON_END 0x19
#define SEQ_INPUT_INTERLOCK_ON_START 0x1A
#define SEQ_INPUT_INTERLOCK_ON_END 0x1F
   
#define SEQ_OUTPUT_OFF_START 0x41
#define SEQ_OUTPUT_OFF_END 0x4D
#define SEQ_OUTPUT_INTERLOCK_OFF_START 0x4E
#define SEQ_OUTPUT_INTERLOCK_OFF_END 0x52

#define SEQ_INPUT_OFF_START 0x53
#define SEQ_INPUT_OFF_END 0x59
#define SEQ_INPUT_INTERLOCK_OFF_START 0x5A
#define SEQ_INPUT_INTERLOCK_OFF_END 0x5F
   
#define SEQ_ENUM_DELAY 0x81
#define SEQ_ENUM_END 0x90
   
#define TAKEOUT_CHECK 1500
#define DELAY_TIME 100

#define SEQ_TIME_WAIT 0xFF
   
   
/* macro ----------------------------------------------------------------------*/

/* function prototypes --------------------------------------------------------*/

/* variables ------------------------------------------------------------------*/

/* ----------------------------------------------------------------------------*/


 extern void LoopAppSeq(void);

#ifdef __cplusplus
}
#endif
#endif /*__APPSEQ_H */