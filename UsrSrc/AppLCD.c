#include "AppLCD.h"
#include "main.h"

void IsBusy(void);
void ClearGLCD(void);
void InitGLCD(void);





void Lcd_Busy_Wait_Ready(uint8_t board);
void Lcd_Display_Start_Line(uint8_t board, uint8_t start_line);
void Lcd_Page_Address_Set(uint8_t board, uint8_t page_addr);
void Lcd_Set_Address(uint8_t board, uint8_t addr);

void Lcd_Write_Display_Data(uint8_t board, uint8_t data);

uint8_t Lcd_Status_Read(uint8_t board);
uint8_t Lcd_Read_Display_Data(uint8_t board);

void GLCDWritePage(void);
void GLCDCmd(uint8_t cs, uint8_t cmd, uint8_t data);
void GLCDOn(void);
void GLCDOff(void);
void GLCDWrite(uint8_t rs,uint8_t cs,uint8_t data);
volatile void Delay_us(uint32_t us);

void LoopAppLCD(void)
{
	static uint64_t TempTick = 0;

  static enum
  {
      LCD_INIT = 0,
		
		LCD_OFF,
		LCD_ON,
		
		LCD_TEST1,
		LCD_TEST2,
		
		LCD_PAGE_WRITE,
	  
	  
	  


  }LCDLoopState = LCD_INIT; 
	switch(LCDLoopState)
	{
		case LCD_INIT:
		HAL_GPIO_WritePin(BACKLIGHT_GPIO_Port, BACKLIGHT_Pin, GPIO_PIN_SET);
			GLCDOn();
			LCDLoopState = LCD_TEST1;//LCD_OFF;LCD_PAGE_WRITE;LCD_TEST1;
			SetSysTick(&TempTick, TIME_LCD_TEST);
		break;
		
		case LCD_OFF:
		if(ChkExpireSysTick(&TempTick))
		{
			GLCDOff();
			SetSysTick(&TempTick, TIME_LCD_TEST);
			LCDLoopState = LCD_ON;
		}
		break;
		case LCD_ON:
		if(ChkExpireSysTick(&TempTick))
		{
			GLCDOn();
			SetSysTick(&TempTick, TIME_LCD_TEST);
			LCDLoopState = LCD_OFF;
		}
		break;
		
		
		case LCD_TEST1:
		if(ChkExpireSysTick(&TempTick))
		{
//			HAL_GPIO_TogglePin(BACKLIGHT_GPIO_Port, BACKLIGHT_Pin);
			GLCDCmd(GLCD_CS1, GLCD_CMD_SET_PAGE, 0x00);
			GLCDCmd(GLCD_CS1, GLCD_CMD_SET_Y_ADDRESS, 0x00);
			GLCDWrite(GLCD_RS_DATA, GLCD_CS1, 0x0F);

			SetSysTick(&TempTick, TIME_LCD_TEST);
			LCDLoopState = LCD_TEST2;
		}
		break;
		case LCD_TEST2:
		if(ChkExpireSysTick(&TempTick))
		{
			GLCDCmd(GLCD_CS1, GLCD_CMD_SET_PAGE, 0x04);
			GLCDCmd(GLCD_CS1, GLCD_CMD_SET_Y_ADDRESS, 0x20);
			GLCDWrite(GLCD_RS_DATA, GLCD_CS1, 0x0F);

			SetSysTick(&TempTick, TIME_LCD_TEST);
			LCDLoopState = LCD_TEST1;
		}
		break;
		
		
		case LCD_PAGE_WRITE:
		if(ChkExpireSysTick(&TempTick))
		{
			GLCDWritePage();
			SetSysTick(&TempTick, TIME_LCD_TEST);
		}
		break;
		
		
	}
}
//
//void ClearGLCD(void);
//void InitGLCD(void)
//{
//	ChooseLeft();
//	WriteCMD(0x00 | GLCD_CMD_DISPLAY_START_LINE);
//	ClearGLCD();
//	WriteCMD(GLCD_DISPLAY_ON);
//	
//	ChooseRight();
//	WriteCMD(0x00 | GLCD_CMD_DISPLAY_START_LINE);
//	ClearGLCD();
//	WriteCMD(GLCD_DISPLAY_ON);
//}
//
//void WriteCMD(uint16_t wcmd)
//{
//
//    uint16_t tmpData, bufPort;
//
//    IsBusy();
//    GLCD_Write();
//    GLCD_RS_LOW();  // operation
//
//    Delay_us(1);
//    bufPort = 0x00FF & GPIOB->ODR;
//    tmpData = (wcmd<<8) & 0xFF00;
//    GPIOB->ODR = bufPort | tmpData;
//	 
//    Delay_us(1); 
//
//    GLCD_ENABLE_HIGH();
//    Delay_us(1);
//    GLCD_ENABLE_LOW();
//    Delay_us(1);
//
//}
//void IsBusy(void)
//{
//
//    GPIO_InitTypeDef GPIO_InitStructure;
//    uint16_t tmpData, bufPort;
//    bufPort = 0x00FF & GPIOB->ODR;
//    tmpData = (0xFF<<8) & 0xFF00;
//    GPIOB->ODR = bufPort | tmpData;
//    Delay_us(5);  
//
//    GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;
//	 GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 
//    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;  // Input 설정
//    GPIO_Init(GPIOB, &GPIO_InitStructure); 
//
//    asm(" NOP "); 
//
//    GLCD_RS_LOW();
//    GLCD_Read();   
//    GLCD_ENABLE_HIGH();
//
//    asm(" NOP ");   
//
//	 while(STATUS_BUSY_CHECK)		 
//	 {
//		 asm(" NOP ");
//	 }
//    GLCD_ENABLE_LOW();
//
//    asm(" NOP ");
//  
//
//    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;  // Output 설정
//
//    GPIO_Init(GPIOB, &GPIO_InitStructure);
//}
//
//
//
///*
//
//
//#define GLCD_RS_HIGH()              GPIOA->BSRR = GPIO_Pin_15
//
//#define GLCD_RS_LOW()               GPIOA->BRR = GPIO_Pin_15
//
// 
//
//#define GLCD_Read()                 GPIOA->BSRR = GPIO_Pin_14
//
//#define GLCD_Write()                GPIOA->BRR = GPIO_Pin_14
//
// 
//
//#define GLCD_ENABLE_HIGH()          GPIOA->BSRR = GPIO_Pin_13
//
//#define GLCD_ENABLE_LOW()           GPIOA->BRR = GPIO_Pin_13
//
// 
//
//#define GLCD_CS1_HIGH()             GPIOB->BSRR = GPIO_Pin_1
//
//#define GLCD_CS1_LOW()              GPIOB->BRR = GPIO_Pin_1
//
// 
//
//#define GLCD_CS2_HIGH()             GPIOB->BSRR = GPIO_Pin_5
//
//#define GLCD_CS2_LOW()              GPIOB->BRR = GPIO_Pin_5
//
// 
//
//#define STATUS_BUSY_CHECK           (GPIOB->IDR & GPIO_Pin_15)
//
//
//void GLCD_WR_DATA(uint16_t wdata)
//
//{
//
//    uint16_t tmpData, bufPort;
//
// 
//
//    GLCD_BUSY_CHECK();
//
// 
//
//    GLCD_Write();
//
//    GLCD_RS_HIGH();
//
//    Delay_us(1);
//
// 
//
//    bufPort = 0x00FF & GPIOB->ODR;
//
//    tmpData = (wdata<<8) & 0xFF00;
//
//    GPIOB->ODR = bufPort | tmpData;
//
//Delay_us(1);
//
// 
//
//    GLCD_ENABLE_HIGH();
//
//    Delay_us(1);
//
//    GLCD_ENABLE_LOW();
//
//    Delay_us(1);
//
// 
//
//}
//
//
//
///* GLCD init -------------------------------------------------------------------*/
//
//void GLCD_init(void)
//
//{
//
//    GLCD_Left_Panel();                            /* select left half panel           */
//
//    GLCD_WR_CMD(0X00|START_LINE);       /* set start lines address 00H     */
//
//    GLCD_ClearRAM();                                         /* clear screen         */
//
//GLCD_WR_CMD(GLCD_Display_ON);                /* display on      */
//
// 
//
//    GLCD_Right_Panel();                          /* select right half panel         */
//
//    GLCD_WR_CMD(0X00|START_LINE);       /* set start lines address 00H     */
//
//    GLCD_ClearRAM();                                         /* clear screen         */
//
//	GLCD_WR_CMD(GLCD_Display_ON);                /* display on      */
//
//}
//
// 
//
///* GLCD Clear Ram -------------------------------------------------------------------*/
//
//void GLCD_ClearRAM(void)
//
//{
//
//    uint8_t x,y; /* x page address */ /* y column address     */
//
// 
//
//    for(x=0;x<8;x++)
//
//           {
//
//                     GLCD_WR_CMD(x|PAGE);              /* set page address        */
//
//                     GLCD_WR_CMD(0X00|COLUMN);    /* set column address */
//
//                     for(y=0;y<64;y++)
//
//                                GLCD_WR_DATA(0x00);    
//
//           }
//
//}
//
////void GLCD_fontTest(void)
////
////{
////
////    uint8_t i, px, py;
////
////   
////
////    px = 0; // init X position
////
////    py = 2; // init Y position
////
//// 
////
////    for(i=0x20;i<0x80;i++)
////
////    {
////
////        if (px<MODM)
////
////            {GLCD_Left_Panel();}                              //x<64 CS1 enable
////
////        else
////
////            {GLCD_Right_Panel();}                            //x>64 CS2 enable
////
////   
////
////        GLCD_putstr(px,py,i);       
////
////        px=px+8;
////
////       
////
////        // Auto Line Add
////
////        if (px >= LCMLIMIT){px=0;py+=1;}                                            
////
////        if (py > 7) py=0;                     
////
////    }
////
////}
//
// 
//
///* GLCD putstr -------------------------------------------------------------------*/
//
////void GLCD_putstr(uint8_t px, uint8_t py, uint8_t one_byte)
////
////{
////	
////	uint8_t k;
////	
////	uint16_t ascii_code;   
////	
////	uint8_t cbyte, col, row;
////	
////	
////	
////	ascii_code=(one_byte-0x20)*8;                                
////	
////	GLCD_WR_CMD(px&0x3F|COLUMN);                                                // col.and.0x3f.or.setx
////	
////	GLCD_WR_CMD(py&0x07|PAGE);                                                     // row.and.0x07.or.sety
////	
////	
////	
////	for(k=0;k<8;k++)
////		
////	{
////		
////		cbyte = Font_8x8[ascii_code];                  
////		
////		GLCD_WR_DATA(cbyte);                                    
////		
////		ascii_code++;     
////		
////	}    
////	
////}
//
//
//
//
//
//
//
//
//
//
//void GLCDWritePage(void)
//{
//	int i,j;
//	static uint8_t testData = 0x00;
//	
//	GLCDCmd(GLCD_CS_BOTH, GLCD_CMD_DISPLAY_START_LINE, 0x3F);
//	
//	for (i = 0 ; i < 8 ; i ++ ) // page
//	{
//		for (j = 0 ; j < 64 ; j ++ ) // address
//		{
//			GLCDCmd(GLCD_CS_BOTH, GLCD_CMD_SET_PAGE, i);
//			GLCDCmd(GLCD_CS_BOTH, GLCD_CMD_SET_Y_ADDRESS, j);
//			GLCDWrite(GLCD_RS_DATA, GLCD_CS_BOTH, testData);
//		}
//	}
//	testData++;
//	
//
//}
//void GLCDCmd(uint8_t cs, uint8_t cmd, uint8_t data)
//{
//	GLCDWrite(GLCD_RS_INSTRUCTION, cs, cmd | data);
//}
//
//void GLCDOn(void)
//{
//	GLCDWrite(GLCD_RS_INSTRUCTION, GLCD_CS_BOTH, GLCD_DISPLAY_ON); // display on;
//}
//void GLCDOff(void)
//{
//	GLCDWrite(GLCD_RS_INSTRUCTION, GLCD_CS_BOTH, GLCD_DISPLAY_OFF); // display off;
//}
//
//
//void GLCDWrite(uint8_t rs,uint8_t cs,uint8_t data)
//{
//	//1. Delay(ns or us)
//	Delay_us(TIME_us_LCD);
//	//Control Port Reset
//	HAL_GPIO_WritePin(LCD_RW_GPIO_Port, LCD_RW_Pin | LCD_RS_Pin | LCD_CS0_Pin | LCD_CS1_Pin, GPIO_PIN_RESET);
////	HAL_GPIO_WritePin(LCD_RW_GPIO_Port, LCD_RW_Pin, GPIO_PIN_RESET);
////	HAL_GPIO_WritePin(LCD_RS_GPIO_Port, LCD_RS_Pin, GPIO_PIN_RESET);
//	HAL_GPIO_WritePin(LCD_EN_GPIO_Port, LCD_EN_Pin, GPIO_PIN_SET);
////	HAL_GPIO_WritePin(LCD_CS0_GPIO_Port, LCD_CS0_Pin, GPIO_PIN_RESET);	
////	HAL_GPIO_WritePin(LCD_CS1_GPIO_Port, LCD_CS1_Pin, GPIO_PIN_RESET);
//	
//
//	Delay_us(TIME_us_LCD);
////	HAL_Delay
//	//E(Enable) Port Reset
//	HAL_GPIO_WritePin(LCD_EN_GPIO_Port, LCD_EN_Pin, GPIO_PIN_RESET);
//	
//	
//	Delay_us(TIME_us_LCD);
//	//R/W Port Reset
//	HAL_GPIO_WritePin(LCD_RW_GPIO_Port, LCD_RW_Pin, GPIO_PIN_RESET);
//	
//	
//	Delay_us(TIME_us_LCD);
//	//RS
//	if(rs == GLCD_RS_INSTRUCTION)
//	{
//		HAL_GPIO_WritePin(LCD_RS_GPIO_Port, LCD_RS_Pin, GPIO_PIN_RESET);
//	}
//	else//rs == GLCD_RS_DATA
//	{
//		HAL_GPIO_WritePin(LCD_RS_GPIO_Port, LCD_RS_Pin, GPIO_PIN_SET);
//	}
//	
//	
//	Delay_us(TIME_us_LCD);
//	//CS 
//	
//	if(cs == GLCD_CS1)
//	{
//		HAL_GPIO_WritePin(LCD_CS0_GPIO_Port, LCD_CS0_Pin, GPIO_PIN_SET);	
//		HAL_GPIO_WritePin(LCD_CS1_GPIO_Port, LCD_CS1_Pin, GPIO_PIN_RESET);
//	}
//	else if(cs == GLCD_CS2)
//	{
//		HAL_GPIO_WritePin(LCD_CS0_GPIO_Port, LCD_CS0_Pin, GPIO_PIN_RESET);	
//		HAL_GPIO_WritePin(LCD_CS1_GPIO_Port, LCD_CS1_Pin, GPIO_PIN_SET);
//	}
//	else
//	{
//		HAL_GPIO_WritePin(LCD_CS0_GPIO_Port, LCD_CS0_Pin, GPIO_PIN_SET);	
//		HAL_GPIO_WritePin(LCD_CS1_GPIO_Port, LCD_CS1_Pin, GPIO_PIN_SET);
//	}
//	
//	
//	Delay_us(TIME_us_LCD);
//	//E Set
//	HAL_GPIO_WritePin(LCD_EN_GPIO_Port, LCD_EN_Pin, GPIO_PIN_SET);
//	
//	Delay_us(TIME_us_LCD);
//	//DATA
//	GLCD_DATA_GPIO_Port->ODR = (GLCD_DATA_GPIO_Port->ODR & 0xFF00) | data;
//	
//	Delay_us(TIME_us_LCD);
//	//E Reset
//	HAL_GPIO_WritePin(LCD_EN_GPIO_Port, LCD_EN_Pin, GPIO_PIN_RESET);
//	
//	Delay_us(TIME_us_LCD);
//	//Delay
//	HAL_GPIO_WritePin(LCD_RW_GPIO_Port, LCD_RW_Pin, GPIO_PIN_SET);
//	
//}
//
//volatile void Delay_us(uint32_t us)
//{  
//	if(us > 1){
//		volatile  uint32_t count=us*10;
//		while(count--); 
//	}else{
//		volatile  uint32_t count=2*10;
//		while(count--); 
//	}
//}
///*
//if(status == 0)
//	{
//       	for(y = top ; y < height ; y++)
//       	{
//       		Lcd_Busy_Wait_Ready(LCD_CS1);
//       		Lcd_Page_Address_Set(LCD_CS1, y);
//       		Lcd_Busy_Wait_Ready(LCD_CS1);
//       		Lcd_Set_Address(LCD_CS1, left);
//
//       		for(x = left ; x < width ; x++)
//       		{
//       			Lcd_Busy_Wait_Ready(LCD_CS1);
//       			Lcd_Write_Display_Data(LCD_CS1, *(volatile uint8_t *)(Display_Scr + (y * 128) + x));
//       		}
//       	}
//	}
//	else if(status == 1)
//	{
//       	for(y = top ; y < height ; y++)
//      	{
//      		Lcd_Busy_Wait_Ready(LCD_CS2);
//       		Lcd_Page_Address_Set(LCD_CS2, y);
//       		Lcd_Busy_Wait_Ready(LCD_CS2);
//       		Lcd_Set_Address(LCD_CS2, (left - 64));
//
//       		for(x = (left - 64) ; x < (width - 64) ; x++)
//       		{
//       			Lcd_Busy_Wait_Ready(LCD_CS2);
//       			Lcd_Write_Display_Data(LCD_CS2, *(volatile uint8_t *)(Display_Scr + (y * 128) + (x + 64)));
//       		}
//       	}
//	}
//	else
//	{
//       	for(y = top ; y < height ; y++)
//       	{
//       		Lcd_Busy_Wait_Ready(LCD_CS1);
//       		Lcd_Page_Address_Set(LCD_CS1, y);
//       		Lcd_Busy_Wait_Ready(LCD_CS1);
//       		Lcd_Set_Address(LCD_CS1, left);
//
//       		for(x = left ; x < 64 ; x++)
//       		{
//       			Lcd_Busy_Wait_Ready(LCD_CS1);
//       			Lcd_Write_Display_Data(LCD_CS1, *(volatile uint8_t *)(Display_Scr + (y * 128) + x));
//       		}
//       	}
//
//       	for(y = top ; y < height ; y++)
//       	{
//       		Lcd_Busy_Wait_Ready(LCD_CS2);
//       		Lcd_Page_Address_Set(LCD_CS2, y);
//       		Lcd_Busy_Wait_Ready(LCD_CS2);
//       		Lcd_Set_Address(LCD_CS2, 0);
//
//       		for(x = 0 ; x < (width - 64) ; x++)
//       		{
//       			Lcd_Busy_Wait_Ready(LCD_CS2);
//       			Lcd_Write_Display_Data(LCD_CS2, *(volatile uint8_t *)(Display_Scr + (y * 128) + (x + 64)));
//       		}
//       	}
//	}
//
//*/
//
//
////
/////****************************************************/
/////*	func:	Lcd_Busy_Wait_Ready						*/
/////*--------------------------------------------------*/
/////*	return	void									*/
/////****************************************************/
////void Lcd_Busy_Wait_Ready(uint8_t board)
////{
////	uint8_t data;
////	uint8_t chk = 0;
////
////	while(chk == 0)
////	{
////		data = Lcd_Status_Read(board);
////		if((data & 0x80) == 0)
////		{
////			chk = 1;
////		}
////	}
////}
////
////
/////****************************************************/
/////*	func:	Lcd_Display_On_Off						*/
/////*--------------------------------------------------*/
/////*	return	void									*/
/////****************************************************/
////void Lcd_Display_On_Off(uint8_t board, uint8_t on_off)
////{
////	uint32_t addr;
////
////	addr = (uint32_t)((LCD_DISPLAY_ON_OFF_ADDR & LCD_CS_BIT_MASK) | board);
////	*(volatile uint8_t *)addr = (uint8_t)(LCD_DISPLAY_ON_OFF_DATA | on_off);
////}
////
////
/////****************************************************/
/////*	func:	Lcd_Display_Start_Line					*/
/////*--------------------------------------------------*/
/////*	return	void									*/
/////****************************************************/
////void Lcd_Display_Start_Line(uint8_t board, uint8_t start_line)
////{
////	uint32_t addr;
////
////	addr = (uint32_t)((LCD_DISPLAY_START_ADDR & LCD_CS_BIT_MASK) | board);
////	*(volatile uint8_t *)addr = (uint8_t)(LCD_DISPLAY_START_DATA | start_line);
////}
////
////
/////****************************************************/
/////*	func:	Lcd_Page_Address_Set					*/
/////*--------------------------------------------------*/
/////*	return	void									*/
/////****************************************************/
////void Lcd_Page_Address_Set(uint8_t board, uint8_t page_addr)
////{
////	uint32_t addr;
////
////	addr = (uint32_t)((LCD_PAGE_ADDR_SET_ADDR & LCD_CS_BIT_MASK) | board);
////	*(volatile uint8_t *)addr = (uint8_t)(LCD_PAGE_ADDR_SET_DATA | page_addr);
////}
////
////
/////****************************************************/
/////*	func:	Lcd_Set_Address							*/
/////*--------------------------------------------------*/
/////*	return	void									*/
/////****************************************************/
////void Lcd_Set_Address(uint8_t board, uint8_t addr)
////{
////	uint32_t write_addr;
////
////	write_addr = (uint32_t)((LCD_SET_ADDRESS_ADDR & LCD_CS_BIT_MASK) | board);
////	*(volatile uint8_t *)write_addr = (uint8_t)(LCD_SET_ADDRESS_DATA | addr);
////}
////
////
/////****************************************************/
/////*	func:	Lcd_Status_Read							*/
/////*--------------------------------------------------*/
/////*	return	uint8_t							*/
/////****************************************************/
////uint8_t Lcd_Status_Read(uint8_t board)
////{
////	uint32_t addr;
////	uint8_t data;
////
////	addr = (uint32_t)((LCD_STATUS_READ_ADDR & LCD_CS_BIT_MASK) | board);
////	data = *(volatile uint8_t *)addr;
////
////	return data;
////}
////
////
/////****************************************************/
/////*	func:	Lcd_Write_Display_Data					*/
/////*--------------------------------------------------*/
/////*	return	void									*/
/////****************************************************/
////void Lcd_Write_Display_Data(uint8_t board, uint8_t data)
////{
////	uint32_t addr;
////
////	addr = (uint32_t)((LCD_WRITE_DISPLAY_DATA_ADDR & LCD_CS_BIT_MASK) | board);
////	*(volatile uint8_t *)addr = data;
////}
////
////
/////****************************************************/
/////*	func:	Lcd_Read_Display_Data					*/
/////*--------------------------------------------------*/
/////*	return	uint8_t							*/
/////****************************************************/
////uint8_t Lcd_Read_Display_Data(uint8_t board)
////{
////	uint32_t addr;
////	uint8_t data;
////
////	addr = (uint32_t)((LCD_READ_DISPLAY_DATA_ADDR & LCD_CS_BIT_MASK) | board);
////	data = *(volatile uint8_t *)addr;
////
////	return data;
////}